import { Module } from '@nestjs/common';
import * as Joi from '@hapi/joi';
import {TypeOrmModule} from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { UsersModule } from './users/users.module';
import { VehiclesModule } from './vehicles/vehicles.module';
import { AuctionsModule } from './auctions/auctions.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        DBHOST: Joi.required(),
        DBPORT: Joi.number(),
        DBUSER: Joi.required(),
        DBPASS: Joi.required(),
        DBNAME: Joi.required()
      })
    }), 
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get<string>('DBHOST'),
        port: configService.get<number>('DBPORT'),
        username: configService.get<string>('DBUSER'),
        password: configService.get<string>('DBPASS'),
        database: configService.get<string>('DBNAME'),
        entities: [__dirname+''+'/**/*.entity{.ts,.js}'],
        autoLoadEntities: true,
        synchronize: true // TEMP: remove it when going to production. 
      }),
      inject: [ConfigService]
    }), UsersModule, VehiclesModule, AuctionsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
