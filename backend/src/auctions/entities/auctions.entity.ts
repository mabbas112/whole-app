import {Entity, PrimaryGeneratedColumn, Column} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class Auctions {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    auction_id: number; // TEMP: not sure if really a number or string.

    @Column({type: 'timestamptz'})
    auction_date: Date;

    @Column({default: 0})
    cars_listed_for_sold: number;

    @Column({default: 0})
    cars_sold: number;

    @Column({default: 0})
    cars_unsold: number;

}
