import {Entity, PrimaryGeneratedColumn, Column} from 'typeorm';

@Entity()
export class VehiclesEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    lot: number;

    @Column()
    vin: string;

    @Column()
    year: number;

    @Column()
    make: string;

    @Column()
    model: string;

    @Column()
    color: string;

    @Column()
    keys: boolean;

    @Column({nullable: true})
    damage_info: string; // TEMP: have doubts here if this would really be a string column.

    @Column()
    reserve_price: number;

    @Column({nullable: true})
    highest_bid_amount: number;

    @Column({nullable: true})
    sold_for: number;

    @Column({nullable: true, type: 'timestamptz'})
    sold_on_date: Date;

    @Column()
    user_id: number; // TEMP: This is the join with users table

    @Column()
    buyer_id: number; // TEMP: This is the join with users table. 

    @Column()
    pictures: string; //TEMP: This will be a join with pictures table. 
}
