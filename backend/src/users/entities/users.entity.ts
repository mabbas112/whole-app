import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

export enum UserTypes {
    INDIVIDUAL = 'individual',
    COMPANY = 'company'
}

@Entity()
export class Users {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "enum", enum: UserTypes, default: UserTypes.INDIVIDUAL })
    user_type: UserTypes;

    @Column({ unique: true })
    member_id: number;

    @Column()
    emaill: string;

    @Column()
    password: string;

    @Column()
    mobile: string;

    @Column()
    first_name: string; // will be company owner first name in case of company

    @Column()
    last_name: string; // will be company owner last name in case of company

    @Column({ nullable: true })
    company_name: string; // required only for company.

    @Column({ nullable: true })
    company_license: string; // required for company only.

    @Column({ nullable: true })
    trn_number: string; // required for company only. 

    @Column({ default: 0 })
    current_balance: number;

    @Column({ default: 0 })
    bidding_limit: number;

    @Column({ nullable: true })
    buying_history: string; // TEMP: Not sure what this is used for.

    @Column({ nullable: true })
    payment_history: string; // TEMP: Not sure what this would be used for.

    @Column({ nullable: true })
    watchlist: string; // TEMP: This will probably be a join with watchlist table 

    @Column({ nullable: true })
    current_bids: string; // TEMP: This will probably be a join with watchlist table 

    @Column({ default: 0 })
    payments_due: number; // TEMP: Not sure if needed here.

    @Column({ nullable: true })
    selling_history: string; // TEMP: This will probably be a join with selling_history table 

    @Column({ nullable: true })
    registration_documents: string; // TEMP: This will probably be a join with documents table 

}
