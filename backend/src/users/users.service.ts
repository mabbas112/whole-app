import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Users } from './entities/users.entity';
import { Repository } from 'typeorm';
import { UserSignupDto } from './dto/user-signup.dto';

@Injectable()
export class UsersService {

    constructor(
        @InjectRepository(Users) private readonly userRepository: Repository<Users>
    ) { }

    createUser(userSignupDto: UserSignupDto): Promise<Users> {
        const user: Users = new Users();
        user.user_type = userSignupDto.user_type;
        user.member_id = userSignupDto.member_id;
        user.emaill = userSignupDto.email;
        user.password = userSignupDto.password;
        user.mobile = userSignupDto.mobile;
        user.first_name = userSignupDto.first_name;
        user.last_name = userSignupDto.last_name;
        user.company_name = userSignupDto.company_name;
        user.company_license = userSignupDto.company_license;
        user.trn_number = userSignupDto.trn_number;
        return this.userRepository.save(user);

    }

    findAllUser(): Promise<Users[]> {
        return this.userRepository.find();
    }

}
