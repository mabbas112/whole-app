import { UsersRefactor1705417290932 } from 'src/migrations/1705417290932-UsersRefactor';
import {DataSource} from 'typeorm';

export default new DataSource({
    type: 'postgres',
    host: '127.0.0.1',
    port: 5432,
    username: 'postgres',
    password: 'qwQW12!@',
    database: 'gulf_auction',
    entities: [],
    migrations: [UsersRefactor1705417290932]
})