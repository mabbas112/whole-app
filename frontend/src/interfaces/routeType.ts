/* eslint-disable @typescript-eslint/no-explicit-any */

export default interface RouteType {
  path: string;
  name: string;
  component: any;
}
