/* eslint-disable @typescript-eslint/no-explicit-any */

export type DashboardCarType = {
  name: string;
  href: string;
  icon: any;
  amount: number;
};
