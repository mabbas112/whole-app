/* eslint-disable @typescript-eslint/no-explicit-any */

import RouteType from "./routeType";


export default interface IRoute {
  props?: any;
  layout: any;
  name:string;
  routes: RouteType[];
}
