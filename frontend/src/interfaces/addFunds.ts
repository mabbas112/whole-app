export type AddFundsType = {
  amount: number;
  method: string;
};
