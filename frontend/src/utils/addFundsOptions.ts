import { Option } from "../interfaces/option";

export const addFundsOptions: Option[] = [
  { value: "10000", label: "10,000 AED" },
  { value: "20000", label: "20,000 AED" },
  { value: "30000", label: "30,000 AED" },
  { value: "40000", label: "40,000 AED" },
  { value: "50000", label: "50,000 AED" },
  { value: "60000", label: "60,000 AED" },
  { value: "70000", label: "70,000 AED" },
  { value: "80000", label: "80,000 AED" },
  { value: "90000", label: "90,000 AED" },
  { value: "100000", label: "100,000 AED" },
];
