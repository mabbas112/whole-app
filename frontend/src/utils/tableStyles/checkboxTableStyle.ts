export const checkboxTableStyle = {
    table: {
      style: {
        overflow: "auto",
        borderStyle: "none",
      },
    },
    headRow: {
      style: {
        fontWeight: "bold",
        fontSize: 16,
        padding: 12,
      },
    },
    rows: {
      style: {
        height: "60px",
        padding: 12,
      },
    },
  };