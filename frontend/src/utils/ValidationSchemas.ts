import * as Yup from "yup";
import { isValidPhoneNumber } from "react-phone-number-input";

export const LoginValidationSchema = Yup.object({
  email: Yup.string()
    .email("Invalid email address")
    .required("Email is required")
    .matches(/^[^\s@]+@[^\s@]+\.[^\s@]+$/, "Invalid email address"),
  password: Yup.string().required("Password is required"),
});

export const BusinessUserValidationSchema = Yup.object({
  firstName: Yup.string().required("Fisrt Name is required"),
  lastName: Yup.string().required("Last Name is required"),
  companyName: Yup.string().required("Company Name is required"),
  licenseNo: Yup.number().required("License Number is required"),
  trnNumber: Yup.number().required("TRN Number is required"),
  companyDocuments: Yup.string().required("Company Document is required"),
  passportImage: Yup.string().required("Passport Image is required"),
});

export const IndividualUserValidationSchema = Yup.object({
  firstName: Yup.string().required("Fisrt Name is required"),
  passportImage: Yup.string().required("Passport Image is required"),
  lastName: Yup.string().required("Last Name is required"),
});

export const CombinedUserValidationSchema = Yup.object({
  email: Yup.string()
    .email("Invalid email address")
    .required("Email is required")
    .matches(/^[^\s@]+@[^\s@]+\.[^\s@]+$/, "Invalid email address"),
  reEmail: Yup.string()
    .required("Confirm Email is required")
    .email("Invalid email address")
    .matches(/^[^\s@]+@[^\s@]+\.[^\s@]+$/, "Invalid email address")
    .oneOf([Yup.ref("email")], "Emails must match"),
  password: Yup.string()
    .required("Password is required")
    .min(8, "Password must be at least 8 characters long")
    .matches(
      /^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d!@#$%^&*()_+]+$/,
      "Password must include at least 1 uppercase letter, 1 number, and 1 symbol"
    ),
  rePassword: Yup.string()
    .required("Confirm Password is required")
    .min(8, "Password must be at least 8 characters long")
    .matches(
      /^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d!@#$%^&*()_+]+$/,
      "Password must include at least 1 uppercase letter, 1 number, and 1 symbol"
    )
    .oneOf([Yup.ref("password")], "Passwords must match"),
  phone: Yup.string()
    .required("Phone is required")
    .test("phone-number-valid", "Phone Number is not valid", (value) =>
      isValidPhoneNumber(value)
    ),
});

export const AddFundsValidationSchema = Yup.object({
  method: Yup.string().required("Method is required"),
  amount: Yup.number().required("Amount is required"),
});

export const paymentDUeValidationSchema = Yup.object({
  method: Yup.string().required("Method is required"),
});
