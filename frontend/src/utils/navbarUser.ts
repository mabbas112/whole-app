import { SignOut } from "./signOut";

export const navbarUser = {
  name: "Tom Cook",
  email: "tom@example.com",
  imageUrl:
    "https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
};
type MenuItemType = {
  id: number;
  name: string;
  link?: string;
  dropdownItems?: {
    name: string;
    link: string;
  }[];
};

export const navigationTabs: MenuItemType[] = [
  { id: 1, name: "Dashboard", link: "/" },
  {
    id: 2,
    name: "Activity",
    link: "/activity",
    dropdownItems: [
      { name: "Proxy bids", link: "/activity/proxybids" },
      { name: "Watchlist", link: "/activity/watchlist" },
      { name: "Lots won", link: "/activity/lotswon" },
      { name: "Lots lost", link: "/activity/lotslost" },
    ],
  },
  {
    id: 3,
    link: "/auctions",
    name: "Auctions",
    dropdownItems: [
      { name: "Todays' auction", link: "/auctions/today" },
      { name: "Upcoming aucitons", link: "/auctions/upcoming" },
    ],
  },
  {
    id: 4,
    link: "/accounting",
    name: "Accounting",
    dropdownItems: [
      { name: "Current balance", link: "/accounting/currentbalance" },
      { name: "Payment due", link: "/accounting/paymentdue" },
      { name: "Payment history", link: "/accounting/paymenthistory" },
      { name: "Add funds", link: "/accounting/addfunds" },
    ],
  },
];
export const userNavigation = [
  { name: "Your Profile", href: "/profile" },
  { name: "Settings", href: "/settings" },
  { name: "Sign out", func: SignOut },
];
