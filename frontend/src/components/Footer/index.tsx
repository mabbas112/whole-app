import { FC } from "react";

const Footer: FC = () => {
  return (
    <div className="mt-auto flex flex-col items-center w-full bg-darkBlue border rounded justify-center text-white p-2">
      <p>Copywirght © 2023 - Present </p>
      <p>Gulf Auction</p>
    </div>
  );
};

export default Footer;
