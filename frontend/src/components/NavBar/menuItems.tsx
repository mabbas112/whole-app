import { FC, useState } from "react";
import { joinClassNames } from "../../utils/joinClasses";
import { Fragment } from "react";
import { Menu, Transition } from "@headlessui/react";
import { ChevronDownIcon } from "@heroicons/react/20/solid";
import { Link, useLocation } from "react-router-dom";
import { navigationTabs } from "../../utils/navbarUser";

const MenuItems: FC = () => {
  const location = useLocation();
  const pathname = location.pathname;
  const [addClass, setAddClass] = useState<number>(0);
  return (
    <>
      <nav className="hidden lg:flex lg:space-x-6 lg:py-2" aria-label="Global">
        {navigationTabs.map((item) =>
          item.dropdownItems ? (
            <Menu
              key={item.id}
              as="div"
              className="relative inline-block text-left"
            >
              <div
                onMouseEnter={() => {
                  setAddClass(item.id);
                }}
                onMouseLeave={() => {
                  setAddClass(0);
                }}
              >
                <Menu.Button
                  className={joinClassNames(
                    item?.link !== "/" && pathname.includes(item?.link || "")
                      ? "bg-[#192453] "
                      : " hover:bg-gray-50  ",
                    "hover:text-[#7c65d1] inline-flex focus:outline-none text-white items-center rounded-md  text-sm font-normal"
                  )}
                >
                  {item.name}
                  <ChevronDownIcon
                    className={`-mr-1 h-5 w-5  ${
                      addClass === item.id ? "text-[#7c65d1]" : "text-white"
                    } hover:text-[#7c65d1] `}
                    aria-hidden="true"
                  />
                </Menu.Button>
              </div>
              <Transition as={Fragment}>
                <Menu.Items className="absolute mt-4 w-40 origin-top-right bg-white shadow-lg items-center focus:outline-none">
                  <div className="z-50">
                    {item.dropdownItems.map((newItem) => {
                      return (
                        <Menu.Item key={newItem.link}>
                          {() => (
                            <Link
                              to={newItem.link}
                              className={`${
                                item?.link !== "/" &&
                                pathname.includes(newItem?.link || "")
                                  ? "bg-darkBlue text-white"
                                  : "text-gray-700 hover:text-gray-900 hover:bg-gray-100"
                              }  block px-4 py-2 text-sm `}
                            >
                              {newItem.name}
                            </Link>
                          )}
                        </Menu.Item>
                      );
                    })}
                  </div>
                </Menu.Items>
              </Transition>
            </Menu>
          ) : (
            <Link
              key={item.id}
              to={item?.link || "#"}
              className={joinClassNames(
                item?.link === pathname ? "bg-[#192453] " : "hover:bg-gray-50 ",
                "inline-flex text-white items-center rounded-md py-2 px-3 text-sm font-normal hover:text-[#7c65d1] hover:border-[#646cff] border border-darkBlue "
              )}
              aria-current={item?.link === pathname ? "page" : undefined}
            >
              {item.name}
            </Link>
          )
        )}
      </nav>
    </>
  );
};

export default MenuItems;
