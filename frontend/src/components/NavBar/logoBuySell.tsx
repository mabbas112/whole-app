import { FC } from "react";
import { Link } from "react-router-dom";
import Logo from "../../assets/logo.svg";

const LogoBuySell: FC = () => {
  return (
    <div className="relative z-10 flex px-2 lg:px-0">
      <div className="flex flex-shrink-0 items-center md:gap-10 gap-4">
        <Link to={`/`}>
          <img className="h-8 w-auto" src={Logo} alt="Your Company" />
        </Link>
        {!location.pathname.includes("signup") &&
        !location.pathname.includes("login") ? (
          <div className="flex items-center md:gap-10 gap-2">
            <Link to={`/buy`} className="text-white font-semibold text-xl">
              Buy
            </Link>
            <Link to={`/sell`} className="text-white font-semibold text-xl">
              Sell
            </Link>
          </div>
        ) : null}
      </div>
    </div>
  );
};
export default LogoBuySell;
