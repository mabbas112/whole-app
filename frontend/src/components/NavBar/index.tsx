import { FC, Fragment } from "react";
import { Disclosure } from "@headlessui/react";
import { Bars3Icon, BellIcon, XMarkIcon } from "@heroicons/react/24/outline";
import { Link, useLocation } from "react-router-dom";
import SearchBox from "./searchBox";
import LogoBuySell from "./logoBuySell";
import { Menu, Transition } from "@headlessui/react";
import {
  navbarUser,
  navigationTabs,
  userNavigation,
} from "../../utils/navbarUser";
import { joinClassNames } from "../../utils/joinClasses";
import NotificationProfileDropdownMenu from "./notificationProfileMenu";
import MenuItems from "./menuItems";
import { ChevronDownIcon } from "@heroicons/react/20/solid";

const NavBar: FC = () => {
  const location = useLocation();
  const pathname = location.pathname;

  return (
    <div>
      <Disclosure
        as="header"
        className="flex bg-darkBlue rounded shadow flex-col"
      >
        {({ open, close }) => (
          <>
            <div className="mx-auto w-full px-2 sm:px-4 lg:divide-y lg:divide-gray-700 lg:px-8">
              <div className="relative flex h-16 justify-between">
                <LogoBuySell />
                {!location.pathname.includes("signup") &&
                  !location.pathname.includes("login") && <SearchBox />}
                {!location.pathname.includes("signup") &&
                !location.pathname.includes("login") ? (
                  <div className="relative z-10 flex items-center lg:hidden">
                    {/* Mobile menu button */}
                    <Disclosure.Button className="relative inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                      <span className="absolute -inset-0.5" />
                      <span className="sr-only">Open menu</span>
                      {open ? (
                        <>
                          <XMarkIcon
                            className="block h-6 w-6"
                            aria-hidden="true"
                          />
                        </>
                      ) : (
                        <Bars3Icon
                          className="block h-6 w-6"
                          aria-hidden="true"
                        />
                      )}
                    </Disclosure.Button>
                  </div>
                ) : null}
                {!location.pathname.includes("signup") &&
                !location.pathname.includes("login") ? (
                  <NotificationProfileDropdownMenu />
                ) : null}
              </div>
              {!location.pathname.includes("signup") &&
              !location.pathname.includes("login") ? (
                <MenuItems />
              ) : null}
            </div>

            {!location.pathname.includes("signup") &&
            !location.pathname.includes("login") ? (
              <Disclosure.Panel
                as="nav"
                className="lg:hidden flex flex-col"
                aria-label="Global"
              >
                <div className="space-y-1 px-2 pb-3 pt-2 flex flex-col">
                  {navigationTabs.map((item) =>
                    item.dropdownItems ? (
                      <Menu
                        key={`${item.id}`}
                        as="div"
                        className="relative inline-block text-left w-full"
                      >
                        <div>
                          <Menu.Button
                            className={joinClassNames(
                              item?.link !== "/" &&
                                pathname.includes(item?.link || "")
                                ? "bg-[#192453] "
                                : " hover:bg-gray-50  ",
                              "hover:text-[#7c65d1] inline-flex text-white justify-between rounded-md py-2 px-3 text-sm font-medium w-full"
                            )}
                          >
                            <p>{item.name}</p>
                            <ChevronDownIcon
                              className="-mr-1 h-5 w-5 text-gray-400"
                              aria-hidden="true"
                            />
                          </Menu.Button>
                        </div>
                        <Transition
                          as={Fragment}
                          enter="transition ease-out duration-100"
                          enterFrom="transform opacity-0 scale-95"
                          enterTo="transform opacity-100 scale-100"
                          leave="transition ease-in duration-75"
                          leaveFrom="transform opacity-100 scale-100"
                          leaveTo="transform opacity-0 scale-95"
                        >
                          <Menu.Items className="absolute z-10 mt-2 w-56 origin-top-right rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                            <div className="py-1">
                              {item.dropdownItems.map((newItem) => {
                                return (
                                  <Menu.Item key={newItem.link}>
                                    {() => (
                                      <Link
                                      onClick={()=>{close()}}
                                        to={newItem.link}
                                        className={`${
                                          item?.link !== "/" &&
                                          pathname.includes(newItem?.link || "")
                                            ? "bg-darkBlue text-white"
                                            : "text-gray-700 hover:text-gray-900 hover:bg-gray-100"
                                        }  block px-4 py-2 text-sm `}
                                      >
                                        {newItem.name}
                                      </Link>
                                    )}
                                  </Menu.Item>
                                );
                              })}
                            </div>
                          </Menu.Items>
                        </Transition>
                      </Menu>
                    ) : (
                      <Link
                        onClick={() => {
                          close();
                        }}
                        key={item.id}
                        to={item?.link || "#"}
                        className={joinClassNames(
                          item?.link === pathname
                            ? "bg-[#192453] "
                            : "hover:bg-gray-50 ",
                          "inline-flex text-white items-center rounded-md py-2 px-3 text-sm font-normal hover:text-[#7c65d1] hover:border-[#646cff] border border-darkBlue "
                        )}
                        aria-current={
                          item?.link === pathname ? "page" : undefined
                        }
                      >
                        {item.name}
                      </Link>
                    )
                  )}
                </div>
                <div className="border-t border-gray-200 pb-3 pt-4">
                  <div className="flex items-center px-4 gap-10">
                    <div className="ml-3">
                      <div className="text-base font-medium text-white">
                        {navbarUser.name}
                      </div>
                      <div className="text-sm font-medium text-gray-500">
                        {navbarUser.email}
                      </div>
                    </div>
                    <button
                      type="button"
                      className="relative ml-auto flex-shrink-0 rounded-full bg-white p-1 text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                    >
                      <span className="absolute -inset-1.5" />
                      <span className="sr-only">View notifications</span>
                      <BellIcon className="h-6 w-6" aria-hidden="true" />
                    </button>
                  </div>
                  <div className="mt-3 space-y-1 px-2">
                    {userNavigation.map((item) =>
                      item.href ? (
                        <Disclosure.Button
                          key={item.name}
                          as="a"
                          href={item.href}
                          className="block rounded-md px-3 py-2 text-base font-medium text-white hover:bg-gray-50 hover:text-gray-900"
                        >
                          {item.name}
                        </Disclosure.Button>
                      ) : (
                        <button
                          key={item.name}
                          type="button"
                          onClick={() => {
                            if (item.func) {
                              item?.func();
                            }
                            close();
                          }}
                          className="flex rounded-md px-3 py-2 text-base font-medium text-white hover:bg-gray-50 hover:text-gray-900 w-full items-start "
                        >
                          {item.name}
                        </button>
                      )
                    )}
                  </div>
                </div>
              </Disclosure.Panel>
            ) : null}
          </>
        )}
      </Disclosure>
    </div>
  );
};

export default NavBar;
