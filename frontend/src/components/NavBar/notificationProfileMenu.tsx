import { FC, Fragment } from "react";
import { BellIcon } from "@heroicons/react/24/outline";
import { Menu, Transition } from "@headlessui/react";
import { navbarUser, userNavigation } from "../../utils/navbarUser";
import { Link } from "react-router-dom";

const NotificationProfileDropdownMenu: FC = () => {
  return (
    <div className="hidden lg:relative lg:z-10 lg:ml-4 lg:flex lg:items-center">
      <button
        type="button"
        className="relative flex-shrink-0 rounded-full bg-darkBlue  p-1 text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
      >
        <span className="absolute -inset-1.5" />
        <span className="sr-only">View notifications</span>
        <BellIcon className="h-6 w-6" aria-hidden="true" />
      </button>

      {/* Profile dropdown */}
      <Menu as="div" className="relative ml-4 flex-shrink-0">
        <div>
          <Menu.Button className="relative flex rounded-full bg-darkBlue focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
            <span className="absolute -inset-1.5" />
            <span className="sr-only">Open user menu</span>
            <p className="text-white">{navbarUser.name}</p>
          </Menu.Button>
        </div>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className="absolute focus:outline-none right-0 z-10 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg  ">
            {userNavigation.map((item) =>
              item.href ? (
                <Menu.Item key={item.name}>
                  {() => (
                    <Link
                      to={item.href}
                      className={"block px-4 py-2 text-sm text-gray-700"}
                    >
                      {item.name}
                    </Link>
                  )}
                </Menu.Item>
              ) : (
                <Menu.Item key={item.name}>
                  {() => (
                    <button
                      onClick={item.func}
                      className={
                        "block px-4 py-2 text-sm text-gray-700 w-full text-left focus:outline-none hover:text-[#747bff]"
                      }
                      type="button"
                    >
                      {item.name}
                    </button>
                  )}
                </Menu.Item>
              )
            )}
          </Menu.Items>
        </Transition>
      </Menu>
    </div>
  );
};

export default NotificationProfileDropdownMenu;
