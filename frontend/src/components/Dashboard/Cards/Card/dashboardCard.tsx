import { FC } from "react";
import { DashboardCarType } from "../../../../interfaces/dashboardCard";
import { Link } from "react-router-dom";

interface Props {
  card: DashboardCarType;
}

const DashboardCard: FC<Props> = ({ card }) => {
  const { name, amount, href, icon: Icon } = card;
  return (
    <div
      key={name}
      className="overflow-hidden rounded-lg bg-white shadow border"
    >
      <div className="p-5">
        <div className="flex items-center">
          <div className="flex-shrink-0">
            <Icon className="h-6 w-6 text-gray-400" aria-hidden="true" />
          </div>
          <div className="ml-5 w-0 flex-1">
            <dl>
              <dt className="truncate text-sm font-medium text-gray-500">
                {name}
              </dt>
              <dd>
                <div className="text-lg font-medium text-gray-900">
                  {amount.toLocaleString()} AED
                </div>
              </dd>
            </dl>
          </div>
        </div>
      </div>
      <div className="bg-gray-50 px-5 py-3">
        <div className="text-sm">
          <Link
            to={href}
            className="font-medium text-cyan-700 hover:text-cyan-900"
          >
            {name === "Bidding limit" ? "Add funds" : "View all"}
          </Link>
        </div>
      </div>
    </div>
  );
};

export default DashboardCard;
