import { FC } from "react";
import DashboardCard from "./Card/dashboardCard";
import { DashboardCarType } from "../../../interfaces/dashboardCard";
import {
  ScaleIcon,
  CheckCircleIcon,
  UserCircleIcon,
} from "@heroicons/react/24/outline";

const cards: DashboardCarType[] = [
  {
    name: "Current balance",
    href: "/accounting/currentbalance",
    icon: ScaleIcon,
    amount: -30659.45,
  },
  {
    name: "Bidding limit",
    href: "/accounting/addfunds",
    icon: UserCircleIcon,
    amount: 19119.45,
  },
  {
    name: "Total Parchases",
    href: "/accounting/paymenthistory",
    icon: CheckCircleIcon,
    amount: 20000,
  },
];

const DashboardOverviewCards: FC = () => {
  return (
    <div className="mt-2 grid grid-cols-1 gap-5 sm:grid-cols-2 lg:grid-cols-3">
      {cards.map((card) => {
        return <DashboardCard card={card} key={card.name} />;
      })}
    </div>
  );
};
export default DashboardOverviewCards;
