import { FC } from "react";
import dayjs from "dayjs";
import { Link } from "react-router-dom";
import TableComponent from "../Common/tableComponent";
import DashboardOverviewCards from "./Cards/dashboardOverviewCards";

type RecentActivityType = {
  id: number;
  lotNumber: number;
  description: string;
  vinNumber: number;
  activity: string;
  auctionDate: string;
};

const RecentActivities: RecentActivityType[] = [
  {
    id: 1,
    lotNumber: 1232356,
    description: "2024 Honda City",
    activity: "You won this lot",
    vinNumber: 211212,
    auctionDate: "01-14-2024",
  },
  {
    id: 2,
    lotNumber: 4563772,
    description: "2017 Toyota Aqua Hybrid",
    activity: "Added to your WatchList",
    vinNumber: 767554,
    auctionDate: "12-28-2023",
  },
  {
    id: 3,
    lotNumber: 9876654,
    description: "2022 Honda Civic",
    activity: "You entered a proxy pid",
    vinNumber: 443311,
    auctionDate: "12-24-2023",
  },
  {
    id: 4,
    lotNumber: 1233221,
    description: "2014 Honda Vezel",
    activity: "Added to WatchList",
    vinNumber: 299898,
    auctionDate: "11-24-2023",
  },
  {
    id: 5,
    lotNumber: 6656541,
    description: "2010 Honda Accord",
    activity: "Added to WatchList",
    vinNumber: 121212,
    auctionDate: "11-20-2023",
  },
  {
    id: 6,
    lotNumber: 4344321,
    description: "Toyota Aqua 2017 Hybrid",
    activity: "You did not win this lot",
    vinNumber: 212323,
    auctionDate: "11-10-2023",
  },
  {
    id: 7,
    lotNumber: 5654432,
    description: "2020 Toyota Corolla",
    activity: "You won this lot",
    vinNumber: 112341,
    auctionDate: "11-04-2023",
  },
  {
    id: 8,
    lotNumber: 9909099,
    description: "2007 Suzuki Mehran",
    activity: "Added to WatchList",
    vinNumber: 876790,
    auctionDate: "10-01-2023",
  },
  {
    id: 9,
    lotNumber: 3450198,
    description: "2008 Suzuki Cultus",
    activity: "Added to WatchList",
    vinNumber: 109876,
    auctionDate: "09-24-2023",
  },
  {
    id: 10,
    lotNumber: 6518092,
    description: "2022 Hyundai Sonata",
    activity: "You did not win this lot",
    vinNumber: 898764,
    auctionDate: "09-02-2023",
  },
  {
    id: 11,
    lotNumber: 8019822,
    description: "2022 Kia Sportage",
    activity: "You did not win this lot",
    vinNumber: 344144,
    auctionDate: "08-08-2023",
  },
  {
    id: 12,
    lotNumber: 7886421,
    description: "2023 Toyota Fortuner Legender",
    activity: "You won this lot",
    vinNumber: 322141,
    auctionDate: "07-23-2023",
  },
];

const columns = [
  {
    name: "Lot#",
    selector: (row: RecentActivityType) => (
      <Link
        to={`#`}
        className="relative text-base text-primary font-bold text-[#3c789d]"
      >
        {row.lotNumber}
      </Link>
    ),
    grow: 1,
  },
  {
    name: "Description",
    selector: (row: RecentActivityType) => (
      <div className="relative content-center items-center truncate text-center">
        <p className="truncate text-base font-normal leading-normal text-black dark:text-white">
          {row?.description}
        </p>
      </div>
    ),
    grow: 4,
  },
  {
    name: "Vin#",
    selector: (row: RecentActivityType) => (
      <div className="relative text-base text-[#9297a0] text-primary">
        {row.vinNumber}
      </div>
    ),
    grow: 1,
  },

  {
    name: "Activity",
    selector: (row: RecentActivityType) => (
      <div className="relative content-center items-center truncate text-center">
        <p className="truncate text-base font-normal leading-normal text-[#9297a0] dark:text-white">
          {row?.activity}
        </p>
      </div>
    ),
    grow: 4,
  },
  {
    name: "Auction Date",
    selector: (row: RecentActivityType) => (
      <p className="  text-base font-normal leading-normal text-gray-text">
        {dayjs(row?.auctionDate).format("DD/MM/YYYY")}
      </p>
    ),
    minWidth: "140px",
  },
];

const DashboardMain: FC = () => {
  return (
    <>
      <div className="min-h-full  border-x p-2">
        <div className="flex flex-1 flex-col ">
          <main className="flex-1 pb-8">
            <h2 className="pt-10 pb-5 px-8 font-bold text-xl">Dashboard</h2>
            <div className="mt-8">
              <div className="mx-auto pb-6 max-w-6xl px-4 sm:px-6 lg:px-8">
                <h2 className="text-lg font-medium leading-6 text-gray-900">
                  Overview
                </h2>
                <DashboardOverviewCards />
              </div>
              <div className="mx-auto max-w-6xl px-4 sm:px-6 lg:px-8 h-full">
                <TableComponent
                  limit={10}
                  tableHeading="Recent activity"
                  paginate={true}
                  spinner={false}
                  totalRecords={RecentActivities.length}
                  currentPage={1}
                  tableColumns={columns}
                  data={RecentActivities}
                />
              </div>
            </div>
          </main>
        </div>
      </div>
    </>
  );
};

export default DashboardMain;
