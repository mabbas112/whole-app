import { FC } from "react";

const NotFound: FC = () => {
  return (
    <>
      <div className="items-center justify-center text-center  p-40">
        <h2 className="font-bold text-2xl text-black">
          Sorry, Page not found.
        </h2>
        <p>The reuqested URL does not exist.</p>
      </div>
    </>
  );
};

export default NotFound;
