import { FC, useState } from "react";
import { useFormik } from "formik";
import type { FormikValues } from "formik";
import InputComponent from "../Common/inputComponent";
import {
  BusinessUserValidationSchema,
  IndividualUserValidationSchema,
} from "../../utils/ValidationSchemas";

interface Props {
  setActiveStep: (step: number) => void;
  registeredUser: string;
}

type BusinessUserFormData = {
  firstName: string;
  lastName: string;
  companyName?: string;
  licenseNo?: number;
  trnNumber?: number;
  passportImage: string;
  companyDocuments?: string;
};

const DefaultUserValues: BusinessUserFormData = {
  firstName: "",
  lastName: "",
  companyName: "",
  passportImage: "",
  companyDocuments: "",
  licenseNo: undefined,
  trnNumber: undefined,
};

const SignupMainForm: FC<Props> = ({ setActiveStep, registeredUser }) => {
  const [businessUser, setBusinessUser] =
    useState<BusinessUserFormData>(DefaultUserValues);

  const getImageBlob = (file: File) => {
    return URL.createObjectURL(file);
  };

  const formik: FormikValues = useFormik({
    initialValues: DefaultUserValues,
    validationSchema:
      registeredUser === "business"
        ? BusinessUserValidationSchema
        : IndividualUserValidationSchema,

    onSubmit: (values) => {
      const user: BusinessUserFormData = {
        firstName: values.firstName,
        lastName: values.lastName,
        companyName: values.companyName,
        licenseNo: values.licenseNo,
        trnNumber: values.trnNumber,
        passportImage: values.passportImage,
        companyDocuments: values.companyDocuments,
      };
      setBusinessUser(user);
      setActiveStep(3);
    },
  });

  console.log("Check FormikData: ", businessUser);

  return (
    <div>
      <div className="space-y-12">
        <div className="grid grid-cols-1 gap-x-8 gap-y-10 border-b border-gray-900/10 pb-12 md:grid-cols-3">
          <div></div>
          <div className="w-full">
            <div className="max-w-[1280px] sm:w-[1280px]">
              <form onSubmit={formik.handleSubmit}>
                <div className="flex flex-col gap-8 w-full">
                  <InputComponent
                    label={`First name ${
                      registeredUser === "business" ? "of company owner" : ""
                    }`}
                    type="text"
                    name="firstName"
                    id="firstName"
                    value={formik.values.firstName}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.errors.firstName}
                    touched={formik.touched.firstName}
                  />
                  <InputComponent
                    label={`Last name ${
                      registeredUser === "business" ? "of company owner" : ""
                    }`}
                    type="text"
                    name="lastName"
                    id="lastName"
                    value={formik.values.lastName}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.errors.lastName}
                    touched={formik.touched.lastName}
                  />

                  {registeredUser === "business" ? (
                    <InputComponent
                      label="Company name"
                      type="text"
                      name="companyName"
                      id="companyName"
                      value={formik.values.companyName}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      error={formik.errors.companyName}
                      touched={formik.touched.companyName}
                    />
                  ) : null}
                  {registeredUser === "business" ? (
                    <InputComponent
                      label="Company license number"
                      type="number"
                      name="licenseNo"
                      id="licenseNo"
                      value={formik.values.licenseNo}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      error={formik.errors.licenseNo}
                      touched={formik.touched.licenseNo}
                    />
                  ) : null}
                  {registeredUser === "business" ? (
                    <InputComponent
                      label="TRN number"
                      type="number"
                      name="trnNumber"
                      id="trnNumber"
                      value={formik.values.trnNumber}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      error={formik.errors.trnNumber}
                      touched={formik.touched.trnNumber}
                    />
                  ) : null}
                  <div className="flex flex-col justify-between  gap-2">
                    <div className="">
                      <label
                        htmlFor="website"
                        className="block text-sm font-medium leading-4 text-gray-900"
                      >
                        Upload passport copy{" "}
                        {registeredUser === "business"
                          ? "of company owner"
                          : ""}
                      </label>
                    </div>
                    <div className=" col-span-full  justify-between items-center flex gap-2">
                      <div className=" flex flex-col  gap-x-3 h-10 ">
                        <label
                          className={`flex  p-1 cursor-pointer rounded ${
                            formik.errors.passportImage &&
                            formik.touched.passportImage
                              ? `border-[#FF0000] border-4 `
                              : `border border-primary`
                          } font-semibold text-primary  h-10 items-center justify-center  px-4`}
                        >
                          <input
                            type="file"
                            id="passportImage"
                            onChange={(e) => {
                              if (e.target.files) {
                                const image = getImageBlob(e?.target?.files[0]);
                                formik.setFieldValue("passportImage", image);
                              }
                            }}
                            name="passportImage"
                            accept="image/*,application/pdf"
                            className="cursor-pointer"
                          />
                          <p className="cursor-pointer border-2 p-[2px] rounded">
                            Browse
                          </p>
                        </label>
                        {formik.errors.passportImage &&
                        formik.touched.passportImage ? (
                          <p className="text-red-600">
                            {formik.errors.passportImage}
                          </p>
                        ) : null}
                      </div>
                    </div>
                  </div>
                  {registeredUser === "business" ? (
                    <div className="flex flex-col justify-between  gap-2">
                      <div className="">
                        <label
                          htmlFor="website"
                          className="block text-sm font-medium leading-4 text-gray-900"
                        >
                          Upload company registered documents
                        </label>
                      </div>
                      <div className="cursor-pointer col-span-full  justify-between items-center flex gap-2">
                        <div className="cursor-pointer flex flex-col  gap-x-3 h-10 ">
                          <label
                            className={`flex  p-1 cursor-pointer rounded ${
                              formik.errors.companyDocuments &&
                              formik.touched.companyDocuments
                                ? `border-[#FF0000] border-4 `
                                : `border border-primary`
                            } font-semibold text-primary  h-10 items-center justify-center  px-4`}
                          >
                            <input
                              type="file"
                              id="companyDocuments"
                              name="companyDocuments"
                              onChange={(e) => {
                                if (e.target.files) {
                                  const image = getImageBlob(
                                    e?.target?.files[0]
                                  );
                                  formik.setFieldValue(
                                    "companyDocuments",
                                    image
                                  );
                                }
                              }}
                              accept="image/*,application/pdf"
                              className="cursor-pointer"
                            />
                            <p className="cursor-pointer border-2 p-[2px] rounded">
                              Browse
                            </p>
                          </label>
                          {formik.touched.companyDocuments &&
                          formik.errors.companyDocuments ? (
                            <p className="text-red-600">
                              {formik.errors.companyDocuments}
                            </p>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  ) : null}
                  <div className=" flex  gap-x-6">
                    <button
                      type="submit"
                      className="rounded-none w-[80px] bg-[#ea7c35] px-3 py-[6px] text-sm font-semibold text-white shadow-sm  focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 "
                    >
                      Next
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignupMainForm;
