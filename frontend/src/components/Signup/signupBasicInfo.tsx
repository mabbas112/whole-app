import { FC, useState } from "react";
import SignupBothForm from "./signupBothForm";
import SignupMainForm from "./signupMainForm";

interface Props {
  setActiveStep: (step: number) => void;
}
const SignupBasicInfo: FC<Props> = ({ setActiveStep }) => {
  const [userType, setUserType] = useState<string>();
  const [mainUserType, setMainUserType] = useState<string>();
  const [userSelected, setUserSelected] = useState<boolean>(false);
  const [registered, setRegistered] = useState<boolean>(false);

  const userFormSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (userType) {
      setUserSelected(true);
      setRegistered(false);
      setMainUserType(userType);
    }
  };

  return (
    <div className="py-10">
      <form onSubmit={userFormSubmit}>
        <div className="space-y-12">
          <div className="grid grid-cols-1 gap-x-8 gap-y-10  pb-12 md:grid-cols-3">
            <div>
              <h2 className="text-base font-semibold leading-7 text-gray-900">
                Basic Information
              </h2>
            </div>

            <div className="grid max-w-2xl grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6 md:col-span-2">
              <div className="sm:col-span-4 space-y-4 flex flex-col items-start">
                <label
                  htmlFor="website"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Are you registering as a business or an individual?
                </label>
                <div className="space-y-2">
                  <div
                    className="flex items-center cursor-pointer"
                    onClick={() => {
                      setUserType("business");
                    }}
                  >
                    <input
                      id={"business"}
                      value={"business"}
                      name="notification-method"
                      type="radio"
                      onChange={(e) => {
                        setUserType(e.target.value);
                      }}
                      checked={userType === "business"}
                      className="h-4 w-4 border-gray-300 text-indigo-600 focus:ring-indigo-600 cursor-pointer"
                    />
                    <label className="ml-3 block text-sm font-medium leading-6 text-gray-900 cursor-pointer">
                      <div className="flex gap-2">
                        <p className="font-medium">Business</p>
                        <p className="text-sm text-[#9fa3ac]">
                          (business license and TRN number required)
                        </p>
                      </div>
                    </label>
                  </div>
                  <div
                    className="flex items-center cursor-pointer"
                    onClick={() => {
                      setUserType("individual");
                    }}
                  >
                    <input
                      id={"individual"}
                      value={"individual"}
                      name="notification-method"
                      type="radio"
                      onChange={(e) => {
                        setUserType(e.target.value);
                      }}
                      checked={userType === "individual"}
                      className="h-4 w-4 border-gray-300 text-indigo-600 focus:ring-indigo-600 cursor-pointer"
                    />
                    <label className="ml-3 block text-sm font-medium leading-6 text-gray-900 cursor-pointer">
                      <div className="flex gap-2">
                        <p className="font-medium">Individual</p>
                        <p className="text-sm text-[#9fa3ac]">
                          (for people with no registered number)
                        </p>
                      </div>
                    </label>
                  </div>
                </div>
                <div className="mt-6 flex items-center justify-end gap-x-6">
                  <button
                    type="submit"
                    className="rounded-none w-[80px] bg-[#ea7c35] px-3 py-[6px] text-sm font-semibold text-white shadow-sm  focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 "
                  >
                    Submit
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
      {userSelected && mainUserType && !registered ? (
        <SignupBothForm
          setRegistered={setRegistered}
          setActiveStep={setActiveStep}
        />
      ) : null}
      {registered && mainUserType ? (
        <SignupMainForm
          setActiveStep={setActiveStep}
          registeredUser={mainUserType}
        />
      ) : null}
    </div>
  );
};
export default SignupBasicInfo;
