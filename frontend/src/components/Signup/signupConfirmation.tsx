import { FC } from "react";

const SignupConfirmation: FC = () => {
  return (
    <div className="py-10">
      <div className="space-y-12">
        <div className="grid grid-cols-1 gap-x-8 gap-y-10  pb-12 md:grid-cols-3">
          <div>
            <h2 className="text-base font-semibold leading-7 text-gray-900">
              Basic Information
            </h2>
          </div>

          <div className="sm:max-w-[600px] max-w-[300px] w-full">
            <p>
              Thank you for signing up! Your application will be processed by
              our admin team, and you'll receive a confirmation email upon
              completion (within 1-2 days).
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
export default SignupConfirmation;
