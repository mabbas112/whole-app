import { FC, useState } from "react";
import { useFormik } from "formik";
import type { FormikValues } from "formik";

import PhoneInput, { isValidPhoneNumber } from "react-phone-number-input";
import InputComponent from "../Common/inputComponent";
import { CombinedUserValidationSchema } from "../../utils/ValidationSchemas";

interface Props {
  setRegistered: (step: boolean) => void;
  setActiveStep: (step: number) => void;
}

type IndividualUserFormData = {
  email: string;
  reEmail: string;
  password: string;
  rePassword: string;
  phone: string;
};

const DefaultUserValues: IndividualUserFormData = {
  email: "",
  reEmail: "",
  password: "",
  rePassword: "",
  phone: "",
};

const SignupBothForm: FC<Props> = ({ setRegistered, setActiveStep }) => {
  const [individualUser, setIndividualUser] =
    useState<IndividualUserFormData>(DefaultUserValues);

  const formik: FormikValues = useFormik({
    initialValues: DefaultUserValues,
    validationSchema: CombinedUserValidationSchema,

    onSubmit: (values) => {
      const user: IndividualUserFormData = {
        email: values.email,
        reEmail: values.reEmail,
        password: values.password,
        rePassword: values.rePassword,
        phone: values.phone,
      };
      setIndividualUser(user);
      setRegistered(true);
      setActiveStep(2);
    },
  });

  console.log(individualUser);
  return (
    <div>
      <div className="space-y-12">
        <div className="grid grid-cols-1 gap-x-8 gap-y-10 border-b border-gray-900/10 pb-12 md:grid-cols-3">
          <div></div>
          <div className="w-full">
            <div className="max-w-[1280px] w-full">
              <form onSubmit={formik.handleSubmit}>
                <div className="flex flex-col gap-8 w-full">
                  <InputComponent
                    label="Email address"
                    type="email"
                    name="email"
                    id="email"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.errors.email}
                    touched={formik.touched.email}
                  />
                  <InputComponent
                    label="Re-enter email address"
                    type="email"
                    name="reEmail"
                    id="reEmail"
                    value={formik.values.reEmail}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.errors.reEmail}
                    touched={formik.touched.reEmail}
                  />
                  <div className="col-span-full w-full">
                    <label
                      htmlFor="website"
                      className="block text-sm font-medium leading-4 text-gray-900"
                    >
                      <div className="flex gap-2 items-center">
                        <p className="font-medium">Create password</p>
                        <p className="text-sm text-[#9fa3ac]">
                          (minimum 8 digits, atleast 1 number, 1 symbol, and 1
                          capital letter)
                        </p>
                      </div>
                    </label>
                    <div className="mt-2">
                      <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                        <input
                          type="password"
                          name="password"
                          value={formik.values.password}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                          id="password"
                          className={` ${
                            formik.errors.password && formik.touched.password
                              ? `border-[#FF0000] border-4 `
                              : `border-tertiary border-0`
                          } block flex-1  bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6`}
                        />
                      </div>
                      {formik.errors.password && formik.touched.password ? (
                        <p className="text-red-600">{formik.errors.password}</p>
                      ) : null}
                    </div>
                  </div>
                  <div className="col-span-full w-full">
                    <label
                      htmlFor="website"
                      className="block text-sm font-medium leading-4 text-gray-900"
                    >
                      Confirm Passowrd
                    </label>
                    <div className="mt-2">
                      <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                        <input
                          type="password"
                          name="rePassword"
                          id="rePassword"
                          value={formik.values.rePassword}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                          className={` ${
                            formik.errors.rePassword &&
                            formik.touched.rePassword
                              ? `border-[#FF0000] border-4 `
                              : `border-tertiary border-0`
                          } block flex-1  bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6`}
                        />
                      </div>
                      {formik.errors.rePassword && formik.touched.rePassword ? (
                        <p className="text-red-600">
                          {formik.errors.rePassword}
                        </p>
                      ) : null}
                    </div>
                  </div>
                  <div className="max-w-[1280px] w-full flex flex-col sm:gap-0 gap-8">
                    <PhoneInput
                      className={`flex sm:flex-row flex-col gap-2 h-10 individualPhone ring-gray-300 `}
                      international
                      id="phone"
                      value={formik.values.phone}
                      onChange={(e) => {
                        formik.setFieldValue("phone", e);
                      }}
                      onBlur={formik.handleBlur}
                      defaultCountry="AE"
                      error={
                        formik.values.phone
                          ? isValidPhoneNumber(formik.values.phone)
                            ? undefined
                            : "Invalid phone number"
                          : "Phone number required"
                      }
                    />
                    {formik.errors.phone && formik.touched.phone ? (
                      <p className="text-red-600">{formik.errors.phone}</p>
                    ) : null}
                  </div>

                  <div className=" flex  gap-x-6">
                    <button
                      type="submit"
                      className="rounded-none w-[80px] bg-[#ea7c35] px-3 py-[6px] text-sm font-semibold text-white shadow-sm  focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 "
                    >
                      Next
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignupBothForm;
