import { FC } from "react";
import CurrentBalanceMainCard from "./Cards/currentBalanceMainCard";

const CurrentBalanceMain: FC = () => {
  return (
    <div className="min-h-full  border-x p-2">
      <h2 className="p-8 font-bold text-xl">Current Balance</h2>
      <div className="mx-auto max-w-[700px] flex flex-col ">
        <div className="overflow-hidden bg-white pb-6">
          <h3 className="text-base font-semibold bg-[#f9fafb] text-gray-900 p-2 border-0">
            Current Balance
          </h3>
          <CurrentBalanceMainCard />
        </div>
      </div>
    </div>
  );
};

export default CurrentBalanceMain;
