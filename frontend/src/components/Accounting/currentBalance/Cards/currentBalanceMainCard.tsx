import { FC } from "react";
import DueToDubaiCard from "./Card/dueToDubaiCard";
import DueToYouCard from "./Card/dueToYouCard";
import BalancesCard from "./Card/balancesCard";
import TotalCard from "./Card/totalCard";

export type BalanceCardType = {
  heading: string;
  value: number;
  headingBold?: boolean;
  valueBold?: boolean;
};

const totalInvoices = 400000;
const totalPaid = 170000;
const totalDueToAuction = totalInvoices - totalPaid;

const totalSales = 100000;
const totalCashedOut = 0;
const totalDueToYou = totalSales - totalCashedOut;

const dueToDubai: BalanceCardType[] = [
  {
    heading: "Total Invoices",
    value: totalInvoices,
  },
  {
    heading: "Total Paid",
    value: totalPaid,
  },
  {
    heading: "TOTAL DUE TO DUBAI ONLINE AUCTIONS",
    value: totalDueToAuction,
    headingBold: true,
    valueBold: true,
  },
];

const dueToYou: BalanceCardType[] = [
  {
    heading: "Total Sales",
    value: totalSales,
  },
  {
    heading: "Cashed-out",
    value: totalCashedOut,
  },
  {
    heading: "TOTAL DUE TO YOU",
    value: totalDueToYou,
    headingBold: true,
    valueBold: true,
  },
];

const balances: BalanceCardType[] = [
  {
    heading: "Balances",
    value: totalDueToAuction,
    headingBold: true,
    valueBold: true,
  },
  {
    heading: "",
    value: totalDueToYou * -1,
    headingBold: true,
    valueBold: true,
  },
];

const CurrentBalanceMainCard: FC = () => {
  return (
    <div className="p-2 pt-4 flex flex-col gap-6 ">
      <DueToDubaiCard dueToDubai={dueToDubai} />
      <DueToYouCard dueToYou={dueToYou} />
      <BalancesCard balances={balances} />
      <TotalCard total={totalDueToAuction - totalDueToYou}/>
    </div>
  );
};

export default CurrentBalanceMainCard;
