import { FC } from "react";
import { BalanceCardType } from "../currentBalanceMainCard";
import CurrentBalanceCard from "./currentBalanceCard";
interface Props {
  balances: BalanceCardType[];
}
const BalancesCard: FC<Props> = ({ balances }) => {
  return (
    <div className="flex flex-col max-w-[470px]">
      {balances?.map((card) => {
        return <CurrentBalanceCard card={card} key={card.heading} />;
      })}
    </div>
  );
};

export default BalancesCard;
