import { FC } from "react";
import { BalanceCardType } from "../currentBalanceMainCard";
import CurrentBalanceCard from "./currentBalanceCard";
import { Link } from "react-router-dom";
interface Props {
  dueToYou: BalanceCardType[];
}
const DueToYouCard: FC<Props> = ({ dueToYou }) => {
  return (
    <div className="block sm:flex items-center justify-between gap-10 border-b-2 border-[#8f8f8f] max-w-[600px] pb-4 w-full">
      <div className=" flex flex-col w-full gap-4 sm:gap-0">
        {dueToYou?.map((card) => {
          return <CurrentBalanceCard card={card} key={card.heading} />;
        })}
      </div>
      <div className="sm:max-w-[90px] w-full sm:text-left text-right">
        <Link to={`#`} className="text-sm  text-[#598bab]">
          View History
        </Link>
      </div>
    </div>
  );
};

export default DueToYouCard;
