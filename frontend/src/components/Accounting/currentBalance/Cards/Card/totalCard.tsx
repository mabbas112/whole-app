import { FC } from "react";

interface Props {
  total: number;
}

const TotalCard: FC<Props> = ({ total }) => {
  return (
    <div className="bg-[#f0f9fe] text-gray-900 p-2 border-0 flex">
      <div
        className={`flex flex-col gap-1 w-full ${
          total === 0 ? "max-w-[470px]" : "max-w-[550px]"
        } `}
      >
        <div className="flex justify-between w-full">
          <p className="uppercase font-bold text-xl">Total =</p>
          <div className="flex gap-2">
            <p className="font-bold">{total.toLocaleString()} AED</p>
            <div className="flex">
              {total === 0 ? null : (
                <button
                  type="submit"
                  className="rounded-none bg-[#ea7c35] text-sm px-2 py-0 font-semibold text-white"
                >
                  {total > 0 ? `Pay Now` : `Cash Out`}
                </button>
              )}
            </div>
          </div>
        </div>
        <p className="text-xs text-gray-500 text-right">
          {total > 0
            ? `Amount you owe to Dubai Online Auctions`
            : total < 0
            ? `Amount Dubai Online Auctions owe to you`
            : "You owe nothing to Dubai Online Auctions and vice versa"}
        </p>
      </div>
    </div>
  );
};

export default TotalCard;
