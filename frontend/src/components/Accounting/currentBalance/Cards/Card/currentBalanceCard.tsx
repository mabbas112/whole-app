import { FC } from "react";
import { BalanceCardType } from "../currentBalanceMainCard";

interface Props {
  card: BalanceCardType;
}
const CurrentBalanceCard: FC<Props> = ({ card }) => {
  return (
    <div className="flex flex-col sm:flex-row justify-between">
      <p className={`${card?.headingBold ? "uppercase font-bold" : ""}`}>
        {card.heading ? card.heading + ":" : ""}
      </p>
      <p className={`${card?.valueBold ? "font-bold" : ""}`}>
        {card.value.toLocaleString()} AED
      </p>
    </div>
  );
};

export default CurrentBalanceCard;
