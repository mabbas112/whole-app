import { FC, RefObject, useEffect, useRef, useState } from "react";
import dayjs from "dayjs";
import { Link } from "react-router-dom";
import TableComponent from "../../Common/tableComponent";
import { checkboxTableStyle } from "../../../utils/tableStyles/checkboxTableStyle";

type PaymentHistoryType = {
  id: number;
  lotNumber: number;
  invoiceDate: string;
  invoiceNumber: number;
  description: string;
  vinNumber: number;
  amount: number;
  paidOn: string;
  method: string;
};

const RecentPayments: PaymentHistoryType[] = [
  {
    id: 1,
    lotNumber: 1232356,
    invoiceDate: "01-16-2024",
    invoiceNumber: 567233,
    description: "2024 Honda City ",
    vinNumber: 211212,
    amount: 23000,
    paidOn: "01-14-2024",
    method: "Wire",
  },
  {
    id: 2,
    lotNumber: 3223341,
    invoiceDate: "01-16-2024",
    invoiceNumber: 567233,
    description: "2024 Honda City",
    vinNumber: 211212,
    amount: 23000,
    paidOn: "01-14-2024",
    method: "Wire",
  },
  {
    id: 3,
    lotNumber: 3223342,
    invoiceDate: "01-16-2024",
    invoiceNumber: 567233,
    description: "2024 Honda City",
    vinNumber: 211212,
    amount: 23000,
    paidOn: "01-14-2024",
    method: "Wire",
  },
  {
    id: 4,
    lotNumber: 3223343,
    invoiceDate: "01-16-2024",
    invoiceNumber: 567233,
    description: "2024 Honda City",
    vinNumber: 211212,
    amount: 23000,
    paidOn: "01-14-2024",
    method: "Wire",
  },
  {
    id: 5,
    lotNumber: 3223344,
    invoiceDate: "01-16-2024",
    invoiceNumber: 567233,
    description: "2024 Honda City",
    vinNumber: 211212,
    amount: 23000,
    paidOn: "01-14-2024",
    method: "Wire",
  },
  {
    id: 6,
    lotNumber: 3223345,
    invoiceDate: "01-16-2024",
    invoiceNumber: 567233,
    description: "2024 Honda City",
    vinNumber: 211212,
    amount: 23000,
    paidOn: "01-14-2024",
    method: "Wire",
  },
  {
    id: 7,
    lotNumber: 3223346,
    invoiceDate: "01-16-2024",
    invoiceNumber: 567233,
    description: "2024 Honda City",
    vinNumber: 211212,
    amount: 23000,
    paidOn: "01-14-2024",
    method: "Wire",
  },
  {
    id: 8,
    lotNumber: 3223347,
    invoiceDate: "01-16-2024",
    invoiceNumber: 567233,
    description: "2024 Honda City",
    vinNumber: 211212,
    amount: 23000,
    paidOn: "01-14-2024",
    method: "Wire",
  },
  {
    id: 9,
    lotNumber: 3223348,
    invoiceDate: "01-16-2024",
    invoiceNumber: 567233,
    description: "2024 Honda City",
    vinNumber: 211212,
    amount: 23000,
    paidOn: "01-14-2024",
    method: "Wire",
  },
  {
    id: 10,
    lotNumber: 3223349,
    invoiceDate: "01-16-2024",
    invoiceNumber: 567233,
    description: "2024 Honda City",
    vinNumber: 211212,
    amount: 23000,
    paidOn: "01-14-2024",
    method: "Wire",
  },
  {
    id: 11,
    lotNumber: 3223378,
    invoiceDate: "01-16-2024",
    invoiceNumber: 567233,
    description: "2024 Honda City",
    vinNumber: 211212,
    amount: 23000,
    paidOn: "01-14-2024",
    method: "Wire",
  },
  {
    id: 12,
    lotNumber: 3223372,
    invoiceDate: "01-16-2024",
    invoiceNumber: 567233,
    description: "2024 Honda City",
    vinNumber: 211212,
    amount: 23000,
    paidOn: "01-14-2024",
    method: "Wire",
  },
  {
    id: 13,
    lotNumber: 3223323,
    invoiceDate: "01-16-2024",
    invoiceNumber: 567233,
    description: "2024 Honda City",
    vinNumber: 211212,
    amount: 23000,
    paidOn: "01-14-2024",
    method: "Wire",
  },
  {
    id: 14,
    lotNumber: 3223123,
    invoiceDate: "01-16-2024",
    invoiceNumber: 567233,
    description: "2024 Honda City",
    vinNumber: 211212,
    amount: 23000,
    paidOn: "01-14-2024",
    method: "Wire",
  },
  {
    id: 15,
    lotNumber: 7823123,
    invoiceDate: "01-16-2024",
    invoiceNumber: 567233,
    description: "2024 Honda City",
    vinNumber: 211212,
    amount: 23000,
    paidOn: "01-14-2024",
    method: "Wire",
  },
  {
    id: 16,
    lotNumber: 1241221,
    invoiceDate: "01-16-2024",
    invoiceNumber: 567233,
    description: "2024 Honda City",
    vinNumber: 211212,
    amount: 23000,
    paidOn: "01-14-2024",
    method: "Wire",
  },
];

const PaymentHistoryMain: FC = () => {
  const checkbox: RefObject<HTMLInputElement> = useRef<HTMLInputElement>(null);

  const [checked, setChecked] = useState<boolean>(false);
  const [indeterminate, setIndeterminate] = useState(false);
  const [selectedPeople, setSelectedPeople] = useState<
    Array<PaymentHistoryType>
  >([]);

  const toggleAll = () => {
    setSelectedPeople(checked || indeterminate ? [] : RecentPayments);
    setChecked(!checked && !indeterminate);
    setIndeterminate(false);
  };

  useEffect(() => {
    const isIndeterminate =
      selectedPeople.length > 0 &&
      selectedPeople.length < RecentPayments.length;
    setChecked(selectedPeople.length === RecentPayments.length);
    setIndeterminate(isIndeterminate);
    if (checkbox?.current) {
      checkbox.current!.indeterminate = isIndeterminate;
    }
  }, [selectedPeople]);

  const columns = [
    {
      name: (
        <input
          type="checkbox"
          className="absolute left-4 top-1/2 -mt-2 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
          ref={checkbox}
          checked={checked}
          onChange={toggleAll}
        />
      ),
      selector: (row: PaymentHistoryType) => (
        <div>
          {selectedPeople.includes(row) && (
            <div className="absolute inset-y-0 left-0 w-0.5 bg-indigo-600" />
          )}
          <input
            type="checkbox"
            className="absolute left-4 top-1/2 -mt-2 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
            value={row.id}
            checked={selectedPeople.includes(row)}
            onChange={(e) =>
              setSelectedPeople(
                e.target.checked
                  ? [...selectedPeople, row]
                  : selectedPeople.filter((p) => p !== row)
              )
            }
          />
        </div>
      ),
      width: "20px",
    },
    {
      name: "Lot#",
      selector: (row: PaymentHistoryType) => (
        <Link
          to={`#`}
          className="relative text-base text-primary font-bold text-[#3c789d]"
        >
          {row.lotNumber}
        </Link>
      ),
      width: "150px",
    },
    {
      name: "Invoice Date",
      selector: (row: PaymentHistoryType) => (
        <p className="  text-base font-normal leading-normal text-gray-text">
          {dayjs(row?.invoiceDate).format("MMM DD, YYYY")}
        </p>
      ),
      width: "130px",
    },
    {
      name: "Invoice#",
      selector: (row: PaymentHistoryType) => (
        <p className="  text-base font-normal leading-normal text-gray-text">
          {row?.invoiceNumber}
        </p>
      ),
      width: "150px",
    },
    {
      name: "Description",
      selector: (row: PaymentHistoryType) => (
        <div className="relative content-center items-center truncate text-center">
          <p className="truncate text-base font-normal leading-normal text-black dark:text-white">
            {row?.description}
          </p>
        </div>
      ),
      minWidth: "200px",
    },
    {
      name: "Vin#",
      selector: (row: PaymentHistoryType) => (
        <div className="relative text-base text-[#9297a0] text-primary">
          {row.vinNumber}
        </div>
      ),
    },
    {
      name: "Amount Paid",
      selector: (row: PaymentHistoryType) => (
        <Link
          to={`#`}
          className="relative text-base text-primary font-bold text-[#3c789d]"
        >
          {row.amount.toLocaleString()} AED
        </Link>
      ),
      width: "130px",
    },
    {
      name: "Paid On",
      selector: (row: PaymentHistoryType) => (
        <p className="  text-base font-normal leading-normal text-gray-text">
          {dayjs(row?.paidOn).format("DD/MM/YYYY")}
        </p>
      ),
      minWidth: "150px",
    },

    {
      name: "Method",
      selector: (row: PaymentHistoryType) => (
        <div className="relative content-center items-center truncate text-center">
          <p className="truncate text-base font-normal leading-normal">
            {row?.method}
          </p>
        </div>
      ),
    },
  ];

  return (
    <>
      <div className="min-h-full  border-x p-2">
        <div className="flex flex-1 flex-col ">
          <main className="flex-1 pb-8">
            <h2 className="pt-10 pb-5 px-8 font-bold text-xl">
              Payment History
            </h2>
            <div className="mt-8">
              <div className="mx-auto  px-4 sm:px-6 lg:px-8 h-full">
                <TableComponent
                  limit={50}
                  paginate={true}
                  spinner={false}
                  totalRecords={RecentPayments.length}
                  currentPage={1}
                  tableColumns={columns}
                  data={RecentPayments}
                  shadow={false}
                  customStyles={checkboxTableStyle}
                />
              </div>
            </div>
          </main>
        </div>
      </div>
    </>
  );
};

export default PaymentHistoryMain;
