import { FC, RefObject, useEffect, useRef, useState } from "react";
import dayjs from "dayjs";
import { Link } from "react-router-dom";
import TableComponent from "../../Common/tableComponent";
import { checkboxTableStyle } from "../../../utils/tableStyles/checkboxTableStyle";
import PaymentDueSideDrawer from "./paymentDueSideDrawer";

export type PaymentDueType = {
  id: number;
  lotNumber: number;
  saleDate: string;
  description: string;
  vinNumber: string;
  invoiceAmount: number;
  balanceDue: number;
};

const RecentPayments: PaymentDueType[] = [
  {
    id: 1,
    lotNumber: 1232356,
    saleDate: "01-16-2024",
    balanceDue: 567233,
    description: "2024 Honda City ",
    vinNumber: "2112ASBDSBDBASJ1212",
    invoiceAmount: 23000,
  },
  {
    id: 2,
    lotNumber: 3223341,
    saleDate: "01-16-2024",
    balanceDue: 567233,
    description: "2024 Honda City",
    vinNumber: "2112ASBDSBDBASJ1212",
    invoiceAmount: 23000,
  },
  {
    id: 3,
    lotNumber: 3223342,
    saleDate: "01-16-2024",
    balanceDue: 567233,
    description: "2024 Honda City",
    vinNumber: "2112ASBDSBDBASJ1212",
    invoiceAmount: 23000,
  },
  {
    id: 4,
    lotNumber: 3223343,
    saleDate: "01-16-2024",
    balanceDue: 567233,
    description: "2024 Honda City",
    vinNumber: "2112ASBDSBDBASJ1212",
    invoiceAmount: 23000,
  },
  {
    id: 5,
    lotNumber: 3223344,
    saleDate: "01-16-2024",
    balanceDue: 567233,
    description: "2024 Honda City",
    vinNumber: "2112ASBDSBDBASJ1212",
    invoiceAmount: 23000,
  },
  {
    id: 6,
    lotNumber: 3223345,
    saleDate: "01-16-2024",
    balanceDue: 567233,
    description: "2024 Honda City",
    vinNumber: "2112ASBDSBDBASJ1212",
    invoiceAmount: 23000,
  },
  {
    id: 7,
    lotNumber: 3223346,
    saleDate: "01-16-2024",
    balanceDue: 567233,
    description: "2024 Honda City",
    vinNumber: "2112ASBDSBDBASJ1212",
    invoiceAmount: 23000,
  },
  {
    id: 8,
    lotNumber: 3223347,
    saleDate: "01-16-2024",
    balanceDue: 567233,
    description: "2024 Honda City",
    vinNumber: "2112ASBDSBDBASJ1212",
    invoiceAmount: 23000,
  },
  {
    id: 9,
    lotNumber: 3223348,
    saleDate: "01-16-2024",
    balanceDue: 567233,
    description: "2024 Honda City",
    vinNumber: "2112ASBDSBDBASJ1212",
    invoiceAmount: 23000,
  },
  {
    id: 10,
    lotNumber: 3223349,
    saleDate: "01-16-2024",
    balanceDue: 567233,
    description: "2024 Honda City",
    vinNumber: "2112ASBDSBDBASJ1212",
    invoiceAmount: 23000,
  },
  {
    id: 11,
    lotNumber: 3223378,
    saleDate: "01-16-2024",
    balanceDue: 567233,
    description: "2024 Honda City",
    vinNumber: "2112ASBDSBDBASJ1212",
    invoiceAmount: 23000,
  },
  {
    id: 12,
    lotNumber: 3223372,
    saleDate: "01-16-2024",
    balanceDue: 567233,
    description: "2024 Honda City",
    vinNumber: "2112ASBDSBDBASJ1212",
    invoiceAmount: 23000,
  },
  {
    id: 13,
    lotNumber: 3223323,
    saleDate: "01-16-2024",
    balanceDue: 567233,
    description: "2024 Honda City",
    vinNumber: "2112ASBDSBDBASJ1212",
    invoiceAmount: 23000,
  },
  {
    id: 14,
    lotNumber: 3223123,
    saleDate: "01-16-2024",
    balanceDue: 567233,
    description: "2024 Honda City",
    vinNumber: "2112ASBDSBDBASJ1212",
    invoiceAmount: 23000,
  },
  {
    id: 15,
    lotNumber: 7823123,
    saleDate: "01-16-2024",
    balanceDue: 567233,
    description: "2024 Honda City",
    vinNumber: "2112ASBDSBDBASJ1212",
    invoiceAmount: 23000,
  },
  {
    id: 16,
    lotNumber: 1241221,
    saleDate: "01-16-2024",
    balanceDue: 567233,
    description: "2024 Honda City",
    vinNumber: "2112ASBDSBDBASJ1212",
    invoiceAmount: 23000,
  },
];

const PaymentDueMain: FC = () => {
  const checkbox: RefObject<HTMLInputElement> = useRef<HTMLInputElement>(null);
  const [modalOpen, setModalOpen] = useState<boolean>(false);

  const [checked, setChecked] = useState<boolean>(false);
  const [indeterminate, setIndeterminate] = useState(false);
  const [selectedInvoices, setSelectedInvoices] = useState<
    Array<PaymentDueType>
  >([]);

  const invoiceTotal = selectedInvoices?.reduce((accumulator, bill) => {
    return accumulator + (bill.invoiceAmount || 0);
  }, 0);

  const toggleAll = () => {
    setSelectedInvoices(checked || indeterminate ? [] : RecentPayments);
    setChecked(!checked && !indeterminate);
    setIndeterminate(false);
  };

  useEffect(() => {
    const isIndeterminate =
      selectedInvoices.length > 0 &&
      selectedInvoices.length < RecentPayments.length;
    setChecked(selectedInvoices.length === RecentPayments.length);
    setIndeterminate(isIndeterminate);
    if (checkbox?.current) {
      checkbox.current!.indeterminate = isIndeterminate;
    }
  }, [selectedInvoices]);

  const columns = [
    {
      name: (
        <input
          type="checkbox"
          className="absolute left-4 top-1/2 -mt-2 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
          ref={checkbox}
          checked={checked}
          onChange={toggleAll}
        />
      ),
      selector: (row: PaymentDueType) => (
        <div>
          {selectedInvoices.includes(row) && (
            <div className="absolute inset-y-0 left-0 w-0.5 bg-indigo-600" />
          )}
          <input
            type="checkbox"
            className="absolute left-4 top-1/2 -mt-2 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
            value={row.id}
            checked={selectedInvoices.includes(row)}
            onChange={(e) =>
              setSelectedInvoices(
                e.target.checked
                  ? [...selectedInvoices, row]
                  : selectedInvoices.filter((p) => p !== row)
              )
            }
          />
        </div>
      ),
      width: "20px",
    },
    {
      name: "Lot#",
      selector: (row: PaymentDueType) => (
        <Link
          to={`#`}
          className="relative text-base text-primary font-bold text-[#3c789d]"
        >
          {row.lotNumber}
        </Link>
      ),
      width: "150px",
    },
    {
      name: "Sale Date",
      selector: (row: PaymentDueType) => (
        <p className="  text-base font-normal leading-normal text-gray-text">
          {dayjs(row?.saleDate).format("MMM DD, YYYY")}
        </p>
      ),
      width: "150px",
    },
    {
      name: "Description",
      selector: (row: PaymentDueType) => (
        <div className="relative content-center items-center truncate text-center">
          <p className="truncate text-base font-normal leading-normal text-black dark:text-white">
            {row?.description}
          </p>
        </div>
      ),
      minWidth: "220px",
    },
    {
      name: "Vin#",
      selector: (row: PaymentDueType) => (
        <div className="relative text-base text-[#9297a0] text-primary">
          {row.vinNumber}
        </div>
      ),
      minWidth: "230px",
    },
    {
      name: "Invoice Amt",
      selector: (row: PaymentDueType) => (
        <Link
          to={`#`}
          className="relative text-base text-primary font-bold text-[#3c789d]"
        >
          {row.invoiceAmount.toLocaleString()} AED
        </Link>
      ),
      width: "130px",
    },
    {
      name: "Balance Due",
      selector: (row: PaymentDueType) => (
        <Link
          to={`#`}
          className="relative text-base text-primary font-bold text-[#3c789d]"
        >
          {row.balanceDue.toLocaleString()} AED
        </Link>
      ),
      width: "130px",
    },

    {
      name: "",
      selector: (row: PaymentDueType) => (
        <div className="relative content-center items-center truncate text-center">
          <button
            onClick={() => {
              setSelectedInvoices([row]);
              setModalOpen(true);
            }}
            type="submit"
            className="rounded-none bg-[#ea7c35] text-sm px-2 py-0 font-semibold text-white"
          >
            {`Pay Now`}
          </button>
        </div>
      ),
    },
  ];

  return (
    <>
      <div className="min-h-full  border-x p-2 ">
        <div className="flex flex-1 flex-col ">
          <main className="flex-1 pb-8">
            <div className="block sm:flex justify-between pr-20 w-full">
              <h2 className="pt-10 pb-5 px-8 font-bold text-base sm:text-xl w-full">
                Payment Due
              </h2>
              <div className="flex items-end justify-end w-full max-w-[200px] mx-auto">
                <button
                  type="submit"
                  className={`rounded-none ${
                    selectedInvoices.length > 0
                      ? "bg-[#ea7c35] text-white font-semibold"
                      : "bg-[#e5e7eb] text-black font-normal"
                  }  w-full text-sm px-2 py-0  text-left`}
                  disabled={selectedInvoices.length <= 0}
                  onClick={() => {
                    setModalOpen(true);
                  }}
                >
                  {`Pay Invoice: ${invoiceTotal.toLocaleString()} AED`}
                </button>
              </div>
            </div>
            <PaymentDueSideDrawer
              open={modalOpen}
              setOpen={setModalOpen}
              selectedInvoices={selectedInvoices}
            />
            <div className="mt-8">
              <div className="mx-auto  px-4 sm:px-6 lg:px-8 h-full">
                <TableComponent
                  limit={50}
                  paginate={true}
                  spinner={false}
                  totalRecords={RecentPayments.length}
                  currentPage={1}
                  tableColumns={columns}
                  data={RecentPayments}
                  shadow={false}
                  customStyles={checkboxTableStyle}
                />
              </div>
            </div>
          </main>
        </div>
      </div>
    </>
  );
};

export default PaymentDueMain;
