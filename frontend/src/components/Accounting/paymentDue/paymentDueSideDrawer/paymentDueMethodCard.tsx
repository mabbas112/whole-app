import { FormikValues } from "formik";
import { FC } from "react";
interface Props {
  pay: { value: string; name: string; description: string };
  formik: FormikValues;
  setMethod: (method: string) => void;
}
const PaymentDueMethodCard: FC<Props> = ({ pay, formik, setMethod }) => {
  return (
    <div
      className="flex gap-2 items-start cursor-pointer"
      key={pay.value}
      onClick={() => {
        formik.setFieldValue("method", pay.value);
        setMethod(pay.value);
      }}
    >
      <div>
        <input
          type="radio"
          value={pay.value}
          name="paymentMethod"
          className="cursor-pointer border bg-red-400"
          onChange={(e) => {
            formik.setFieldValue("method", e.target.value);
            setMethod(e.target.value);
          }}
          checked={formik.values.method === pay.value}
        />
      </div>
      <div
        className={`${
          formik.errors.method && formik.touched.method ? "text-red-600" : ""
        }`}
      >
        <p className="font-medium"> {pay.name}</p>
        <p className={`text-gray-500`}>{pay.description}</p>
      </div>
    </div>
  );
};

export default PaymentDueMethodCard;
