import { FC } from "react";

interface Props {
  totalInvoices: number;
  invoicesSum: number;
}
const PaymentSummary: FC<Props> = ({ totalInvoices, invoicesSum }) => {
  return (
    <div className="flex flex-col gap-2 border-b p-6">
      <h2 className="font-bold">Payment Summary</h2>
      <div className="flex flex-col gap-2">
        <div className="flex gap-2 max-w-[250px] w-full justify-between">
          <p>
            {totalInvoices} Invoice
            {totalInvoices === 1 ? "" : "s"} ={" "}
          </p>
          <p>{invoicesSum.toLocaleString()} AED</p>
        </div>
        <div className="flex gap-2 font-bold max-w-[250px] w-full justify-between">
          <p className=" font-bold">TOTAL = </p>
          <p className=" font-bold">{invoicesSum.toLocaleString()} AED</p>
        </div>
      </div>
    </div>
  );
};

export default PaymentSummary;
