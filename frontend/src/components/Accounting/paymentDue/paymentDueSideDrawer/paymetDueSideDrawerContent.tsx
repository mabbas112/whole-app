import { FC, useRef, useState } from "react";
import { useFormik } from "formik";
import type { FormikValues } from "formik";
import { useNavigate } from "react-router-dom";
import { paymentDUeValidationSchema } from "../../../../utils/ValidationSchemas";
import { PaymentDueType } from "..";
import PaymentSummary from "./paymentSummary";
import ReviewPayment from "./reviewPayment";
import PaymentDueMethodCard from "./paymentDueMethodCard";

const paymentTypes = [
  {
    name: "Available Funds",
    value: "availableFunds",
    description: "70,000 AED available",
  },
  {
    name: "Visa Credit Card",
    value: "visa",
    description: "Ending in ...0232 (3% fee will be added if this method used)",
  },
  {
    name: "Check",
    value: "check",
    description: "Bring check to Burj Khaibar, Sajja branch",
  },
  {
    name: "Cash",
    value: "cash",
    description: "Bring cash to Burj Khaibar, Sajja branch",
  },
  {
    name: "Wire Transfer",
    value: "wire",
    description: "Write your member ID# and ref# 1788493 in description",
  },
];
interface Props {
  setOpen: (op: boolean) => void;
  selectedInvoices: PaymentDueType[];
}

const PaymentDueSideDrawerContent: FC<Props> = ({
  setOpen,
  selectedInvoices,
}) => {
  const navigate = useNavigate();
  const formRef = useRef<HTMLDivElement>(null);

  const [method, setMethod] = useState<string>("");

  const invoiceTotal = selectedInvoices?.reduce((accumulator, bill) => {
    return accumulator + (bill.invoiceAmount || 0);
  }, 0);

  const formik: FormikValues = useFormik({
    initialValues: { method: "" },
    validationSchema: paymentDUeValidationSchema,

    onSubmit: (values) => {
      setMethod(values.method);
      navigate(`/`);
    },
  });

  const scrollIntoView = () => {
    if (method.length <= 0) {
      formRef?.current?.scrollIntoView({
        behavior: "smooth",
        block: "center",
        inline: "start",
      });
    }
  };

  return (
    <>
      <div className="flex-1 pb-8">
        <div className="mx-auto pb-6 max-w-[700px] flex flex-col gap-6">
          <div className="flex flex-col gap-2  py-4 px-6">
            <div className=" pb-2">
              <h4 className="font-bold pb-4">Payment method</h4>
              <div
                className={`flex flex-col sm:gap-4 gap-2 px-0 sm:px-4 `}
                ref={formRef}
              >
                {paymentTypes.map((pay) => {
                  return (
                    <PaymentDueMethodCard
                      key={pay.value}
                      pay={pay}
                      formik={formik}
                      setMethod={setMethod}
                    />
                  );
                })}
                <p className="text-red-600 font-bold h-6">
                  {formik.touched.method && formik.errors.method
                    ? formik.errors.method
                    : ""}
                </p>
              </div>
            </div>
          </div>

          <div className="flex flex-col gap-2 border-y-2 p-6">
            <h2 className="font-bold">Review Payment</h2>
            <div className="flex flex-col gap-2">
              {selectedInvoices.map((invoice) => {
                return <ReviewPayment invoice={invoice} key={invoice.id} />;
              })}
            </div>
          </div>

          <PaymentSummary
            invoicesSum={invoiceTotal}
            totalInvoices={selectedInvoices.length}
          />

          <div className="flex gap-4 p-4 justify-end">
            <button
              className="border  border-gray-300"
              type="button"
              onClick={() => {
                setOpen(false);
              }}
            >
              Cancel
            </button>
            <button
              className="border border-gray-300 bg-[#546ae0] text-white"
              type="submit"
              onClick={() => {
                formik.handleSubmit();
                scrollIntoView();
              }}
            >
              {method === "wire" ? `Next` : `Pay Now`}
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default PaymentDueSideDrawerContent;
