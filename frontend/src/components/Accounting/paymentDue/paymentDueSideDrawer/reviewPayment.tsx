import { FC } from "react";
import { PaymentDueType } from "..";
import CorollaImage from "../../../../assets/corolla.jpg";

interface Props {
  invoice: PaymentDueType;
}

const year = "2020";
const make = "Toyota";
const modal = "Corolla";
const odometerReading = 63455;
const driveTrain = 2;
const engine = "4 Cyl Turbo";
const engineType = "Manual";
const lotNo = "11231212";
const vinNo = "1FA767BASHD812AS356676";

const ReviewPayment: FC<Props> = ({ invoice }) => {
  return (
    <div className="block sm:flex gap-2 justify-between">
      <div className="flex gap-2 flex-wrap sm:flex-nowrap">
        <div className="max-w-[200px] h-[145px] w-full">
          <img
            src={CorollaImage}
            alt={`${invoice.id}`}
            className="w-[200px] h-[145px]"
          />
        </div>
        <div className="flex flex-col gap-4">
          <div>
            <div>
              <p className="uppercase font-bold text-[#015987]">
                {year} {make} {modal}
              </p>
            </div>
            <p className="text-sm">
              {odometerReading.toLocaleString()} mi - {driveTrain}WD - {engine}{" "}
              - {engineType}
            </p>
          </div>
          <div>
            <p className="text-sm text-gray-500">lot# {lotNo}</p>
            <p className="text-sm text-gray-500">Vin# {vinNo}</p>
          </div>
        </div>
      </div>
      <div className="">
        <p className="text-gray-600">Invoice Amount</p>
        <p className="font-bold">
          {invoice.invoiceAmount.toLocaleString()} AED{" "}
        </p>
      </div>
    </div>
  );
};

export default ReviewPayment;
