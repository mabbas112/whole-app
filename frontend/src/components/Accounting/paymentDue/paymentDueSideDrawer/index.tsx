import { FC } from "react";
import { PaymentDueType } from "..";
import SideDrawer from "../../../Common/Modals/sideDrawer";
import PaymentDueSideDrawerContent from "./paymetDueSideDrawerContent";

interface Props {
  open: boolean;
  setOpen: (op: boolean) => void;
  selectedInvoices: PaymentDueType[];
}

const PaymentDueSideDrawer: FC<Props> = ({
  open,
  setOpen,
  selectedInvoices,
}) => {
  return (
    <SideDrawer open={open} setOpen={setOpen} heading={`Make Payment`}>
      <PaymentDueSideDrawerContent
        setOpen={setOpen}
        selectedInvoices={selectedInvoices}
      />
    </SideDrawer>
  );
};

export default PaymentDueSideDrawer;
