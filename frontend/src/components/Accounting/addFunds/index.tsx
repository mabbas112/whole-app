import Select from "react-select";
import { FC, useState } from "react";
import { addFundsOptions } from "../../../utils/addFundsOptions";
import { Option } from "../../../interfaces/option";
import { useFormik } from "formik";
import type { FormikValues } from "formik";
import { AddFundsValidationSchema } from "../../../utils/ValidationSchemas";
import { AddFundsType } from "../../../interfaces/addFunds";
import { useNavigate } from "react-router-dom";

const BiddingLimit = 100000;

const DefaultFundsValue: AddFundsType = {
  method: "",
  amount: 10000,
};

const paymentTypes = [
  {
    name: "Visa Credit Card",
    value: "visa",
    description: "Ending in ...0232 (3% fee will be added if this method used)",
  },
  {
    name: "Check",
    value: "check",
    description: "Bring check to Burj Khaibar, Sajja branch",
  },
  {
    name: "Cash",
    value: "cash",
    description: "Bring cash to Burj Khaibar, Sajja branch",
  },
  {
    name: "Wire Transfer",
    value: "wire",
    description: "Write your member ID# and ref# 1788493 in description",
  },
];

const AddFundsMain: FC = () => {
  const navigate = useNavigate();
  const [selectedFunds, setSelectedFunds] = useState<Option>({
    value: "10000",
    label: "10,000 AED",
  });
  const [funds, setFunds] = useState<AddFundsType>(DefaultFundsValue);

  const formik: FormikValues = useFormik({
    initialValues: DefaultFundsValue,
    validationSchema: AddFundsValidationSchema,

    onSubmit: (values) => {
      const fund: AddFundsType = {
        amount: values.amount,
        method: values.method,
      };
      setFunds(fund);
      navigate(`/`);
    },
  });

  console.log({ funds });
  return (
    <>
      <div className="min-h-full  border-x p-2">
        <div className="flex flex-1 flex-col ">
          <div className="flex-1 pb-8">
            <h2 className="p-8 font-bold text-xl">Add funds</h2>
            <div className="mx-auto pb-6 max-w-[700px] flex flex-col gap-6">
              <p>
                Your bidding limit is the most you can spend on auction items.
                The total of all your purchases can't go beyond this limit. If
                your total purchases and current bids surpass your limit, you
                won't be allowed to place more bids.
              </p>
              <div className="border p-4 bg-sky-50">
                <p className="font-bold">
                  Your bidding limit: AED {BiddingLimit.toLocaleString()}
                </p>
              </div>
              <p className="">
                To increase your limit, you must add more funds via one of the
                below methods
              </p>
              <div className="flex flex-col gap-2">
                <p>Amount</p>
                <div className={` w-full sm:w-[382px] `}>
                  <Select
                    isSearchable={false}
                    value={{
                      value: selectedFunds.value,
                      label: selectedFunds.label,
                    }}
                    onChange={(option) => {
                      if (option) {
                        setSelectedFunds(option);
                        formik.setFieldValue("amount", parseInt(option.value));
                      }
                    }}
                    options={addFundsOptions}
                    className="h-[52px]"
                  />
                </div>
              </div>
              <div className="flex flex-col gap-2 border py-4 px-6">
                <div className="border-b-[3px] pb-2">
                  <h4 className="font-bold pb-4">Payment method</h4>
                  <div className={`flex flex-col sm:gap-4 gap-2 px-0 sm:px-4 `}>
                    {paymentTypes.map((pay) => {
                      return (
                        <div
                          className="flex gap-2 items-start cursor-pointer"
                          key={pay.value}
                          onClick={() => {
                            formik.setFieldValue("method", pay.value);
                          }}
                        >
                          <div>
                            <input
                              type="radio"
                              value={pay.value}
                              name="paymentMethod"
                              className="cursor-pointer border bg-red-400"
                              onChange={(e) => {
                                formik.setFieldValue("method", e.target.value);
                              }}
                              checked={formik.values.method === pay.value}
                            />
                          </div>
                          <div
                            className={`${
                              formik.errors.method && formik.touched.method
                                ? "text-red-600"
                                : ""
                            }`}
                          >
                            <p className="font-medium"> {pay.name}</p>
                            <p className={`text-gray-500`}>{pay.description}</p>
                          </div>
                        </div>
                      );
                    })}

                    <p className="text-red-600 font-bold h-6">
                      {formik.touched.method && formik.errors.method
                        ? formik.errors.method
                        : ""}
                    </p>
                  </div>
                </div>
                <div className="border-b-2 p-3">
                  <h4 className="font-bold pb-4">Payment summary</h4>
                  <p className="font-bold text-xl">
                    TOTAL = {selectedFunds.label}
                  </p>
                </div>
                <div className="flex gap-4 p-4 justify-end">
                  <button
                    className="border  border-gray-300"
                    type="button"
                    onClick={() => {
                      navigate(`/`);
                    }}
                  >
                    Cancel
                  </button>
                  <button
                    className="border border-gray-300 bg-[#546ae0] text-white"
                    type="button"
                    onClick={formik.handleSubmit}
                  >
                    Pay Now
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AddFundsMain;
