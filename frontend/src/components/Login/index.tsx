import { FC, useState } from "react";
import { useFormik } from "formik";
import type { FormikValues } from "formik";
import InputComponent from "../Common/inputComponent";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import { LoginValidationSchema } from "../../utils/ValidationSchemas";

type LoginFormData = {
  email: string;
  password: string;
  remember: boolean;
};

const DefaulFormValues: LoginFormData = {
  email: "",
  password: "",
  remember: false,
};

const LoginMainForm: FC = () => {
  const [user, setUser] = useState<LoginFormData>(DefaulFormValues);
  const navigate = useNavigate();

  const formik: FormikValues = useFormik({
    initialValues: DefaulFormValues,
    validationSchema: LoginValidationSchema,

    onSubmit: (values) => {
      const user: LoginFormData = {
        email: values.email,
        password: values.password,
        remember: values.remember,
      };
      setUser(user);
      navigate("/");
    },
  });

  console.log("Check FormikData: ", user);

  return (
    <div>
      <div className="space-y-12">
        <div className="grid grid-cols-1 gap-x-8 gap-y-10 border-b border-gray-900/10 pb-12 md:grid-cols-3">
          <div></div>
          <div className="w-full">
            <div className="max-w-[1280px] sm:w-[1280px]">
              <form onSubmit={formik.handleSubmit}>
                <div className="flex flex-col gap-8 w-full">
                  <InputComponent
                    label={`Email`}
                    type="email"
                    name="email"
                    id="email"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.errors.email}
                    touched={formik.touched.email}
                  />
                  <InputComponent
                    label={`Passowrd`}
                    type="password"
                    name="password"
                    id="password"
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.errors.password}
                    touched={formik.touched.password}
                  />
                  <div className="col-span-full flex  justify-between max-w-[450px]">
                      <div className="flex gap-2 rounded-md  sm:max-w-md">
                        <input
                          type="checkbox"
                          name="remember"
                          id="remember"
                          value={formik.values.remember}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                        />
                        <label>Remeber me</label>
                      </div>
                      <Link to={`#`}>Forgot Password?</Link>
                  </div>

                  <div className=" flex  gap-x-6">
                    <button
                      type="submit"
                      className="rounded-none w-[80px] bg-[#ea7c35] px-3 py-[6px] text-sm font-semibold text-white shadow-sm  focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 "
                    >
                      Log in
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginMainForm;
