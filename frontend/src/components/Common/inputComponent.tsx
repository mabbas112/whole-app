import { FC } from "react";

interface Props {
  value: string | number;
  label:string;
  type: string;
  id: string;
  name: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  onBlur?: React.FocusEventHandler<HTMLInputElement>;
  error?: string;
  touched?: boolean;
  className?:string;
  accept?:string;
}
const InputComponent: FC<Props> = ({
  label,
  value,
  type,
  name,
  id,
  onBlur,
  onChange,
  error,
  touched,
  className,
  accept
}) => {
  return (
    <div className="col-span-full ">
      <label
        htmlFor="website"
        className="block text-sm font-medium leading-4 text-gray-900"
      >
        {label}
      </label>
      <div className="mt-2">
        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
          <input
            type={type}
            name={name}
            id={id}
            value={value}
            onChange={onChange}
            onBlur={onBlur}
            accept={accept}
            className={` ${
              error && touched
                ? `border-[#FF0000] border-4 `
                : `border-tertiary border-0`
            } ${className} block flex-1  bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6`}
          />
        </div>
        {error && touched ? <p className="text-red-600">{error}</p> : null}
      </div>
    </div>
  );
};
export default InputComponent;
