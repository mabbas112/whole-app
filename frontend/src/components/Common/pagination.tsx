import { FC } from "react";
interface Props {
  totalRecords: number;
  currentPage: number;
  limit: number;
  handlePageClick: (page: number) => void;
}
const Pagination: FC<Props> = ({
  totalRecords,
  currentPage,
  handlePageClick,
  limit,
}) => {
  return (
    <div
      className="flex items-center justify-between w-full border-gray-200 bg-white sm:px-6"
      aria-label="Pagination"
    >
      <div className="hidden sm:block">
        <p className="text-sm text-gray-700 flex gap-1">
          <span>Showing</span>
          <span className="font-medium">{(currentPage - 1) * 10 + 1}</span>{" "}
          <span>to</span>
          <span className="font-medium">
            {totalRecords < limit ? totalRecords : currentPage * limit}
          </span>
          <span>of</span>
          <span className="font-medium">{totalRecords}</span>{" "}
          <span>results</span>
        </p>
      </div>
      <div className="w-full flex flex-col gap-2">
        <div className="flex flex-1 sm:pr-0 pr-4 justify-between gap-x-3 sm:justify-end items-center">
          <button
            onClick={() => {
              handlePageClick(currentPage - 1);
            }}
            disabled={currentPage === 1}
            type="button"
            className={`${
              currentPage === 1 ? "opacity-50" : "hover:ring-gray-400"
            } relative inline-flex items-center rounded-md focus:outline-none bg-white px-3 py-2 text-sm font-semibold text-gray-900 ring-1 ring-inset ring-gray-300 `}
          >
            Previous
          </button>
          <button
            onClick={() => {
              handlePageClick(currentPage + 1);
            }}
            disabled={currentPage * limit >= totalRecords}
            type="button"
            className={` ${
              currentPage * limit >= totalRecords
                ? "opacity-50"
                : "hover:ring-gray-400"
            } relative inline-flex items-center rounded-md focus:outline-none  bg-white px-3 py-2 text-sm font-semibold text-gray-900 ring-1 ring-inset ring-gray-300 `}
          >
            Next
          </button>
        </div>
        <div className="sm:hidden block ">
          <div className="text-sm text-gray-700 flex gap-1 justify-center">
            <span>Showing</span>
            <span className="font-medium">
              {(currentPage - 1) * 10 + 1}
            </span>{" "}
            <span>to</span>
            <span className="font-medium">
              {totalRecords < limit ? totalRecords : currentPage * limit}
            </span>
            <span>of</span>
            <span className="font-medium">{totalRecords}</span>{" "}
            <span>results</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Pagination;
