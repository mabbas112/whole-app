import { FC } from "react";
import DataTable from "react-data-table-component";
import Pagination from "./pagination";

const customStylesSelf = {
  table: {
    style: {
      overflow: "auto",
      borderStyle: "none",
    },
  },
  headRow: {
    style: {
      fontWeight: "bold",
      fontSize: 16,
      background: "#f9fafb",
      padding: 12,
    },
  },
  rows: {
    style: {
      height: "60px",
      padding: 12,
    },
  },
};

export type TableComponentProps = {
  paginate: boolean;
  totalRecords: number;
  tableColumns: object[];
  data: object[];
  currentPage?: number;
  setOffset?: (index: number) => void;
  spinner?: boolean;
  tableHeading?: string;
  customStyles?: object;
  shadow?: boolean;
  customClass?: string;
  limit: number;
};

const TableComponent: FC<TableComponentProps> = ({
  paginate,
  totalRecords = 0,
  tableColumns,
  data,
  setOffset,
  currentPage = 1,
  spinner,
  tableHeading,
  customStyles,
  shadow = true,
  customClass = "",
  limit,
}) => {
  const handlePageChange = (selected: number) => {
    if (setOffset) {
      setOffset(selected * (limit || 10));
    }
  };
  return (
    <div className="w-full h-full">
      {tableHeading ? (
        <div
          className={`items-center justify-between  bg-white py-2  text-base font-medium leading-[110%] text-black`}
        >
          {tableHeading}
        </div>
      ) : null}
      <div
        className={`w-full overflow-hidden ${
          shadow
            ? "rounded-md border border-[#EBEEF6] shadow-md"
            : "rounded-t-md"
        } ${customClass}`}
      >
        {spinner ? (
          <div className="relative my-40 text-center">Loading..</div>
        ) : data?.length > 0 ? (
          <DataTable
            customStyles={customStyles || customStylesSelf}
            columns={tableColumns}
            data={data}
          />
        ) : (
          <div className="p-4 text-xl font-bold">You don't have any data.</div>
        )}

        {/* Pagination and Page Count */}
        {paginate && totalRecords !== 0 && (
          <div
            className={`flex items-center justify-between border-t border-[#EBEEF6] bg-white pb-3 pt-5  ${
              spinner ? "mt-3" : ""
            }`}
          >
            <Pagination
              limit={limit}
              totalRecords={totalRecords}
              handlePageClick={handlePageChange}
              currentPage={currentPage}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default TableComponent;
