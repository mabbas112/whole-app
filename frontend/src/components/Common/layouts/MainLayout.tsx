import { FC } from "react";
import NavBar from "../../NavBar";
import { Outlet } from "react-router-dom";
import Footer from "../../Footer";

const MainLayout: FC = () => {
  return (
    <div className="flex flex-col gap-2 ">
      <NavBar />
      <Outlet />
      <Footer />
    </div>
  );
};
export default MainLayout;
