import { Routes, Route, BrowserRouter } from "react-router-dom";
import "./App.css";
import routes from "./config/routes";
import IRoute from "./interfaces/route";
import { FC } from "react";

interface Props {
  props?: React.FunctionComponent;
}
const App: FC<Props> = ({ props }) => {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          {routes.map((route: IRoute, index) => {
            const {layout:Layout} = route;
            return (
              <Route element={<Layout />} key={`${route.layout}${index}`}>
                {route.routes.map((newRoute) => {
                  const {name, path, component:Component} = newRoute;
                  return (
                    <Route
                      key={`${newRoute.path}${index}`}
                      path={path}
                      element={
                        <Component
                          name={name}
                          {...props}
                          {...route.props}
                        />
                      }
                    />
                  );
                })}
              </Route>
            );
          })}
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default App;
