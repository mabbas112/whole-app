import { FC } from "react";
import SignupSteps from "../components/Signup/signupSteps";

const Signup: FC = () => {
  return (
    <div>
      <div className="px-5 flex flex-col gap-4">
        <h4 className="font-bold text-xl ">Sign-up</h4>
        <SignupSteps />
      </div>
    </div>
  );
};

export default Signup;
