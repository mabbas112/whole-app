import { FC } from "react";
import DashboardMain from "../components/Dashboard";

const DashBoard: FC = () => {
  return <DashboardMain />;
};

export default DashBoard;
