import { FC } from "react";
import PaymentHistoryMain from "../../components/Accounting/paymentHistory";

const PaymentHistory: FC = () => {
  return <PaymentHistoryMain />;
};

export default PaymentHistory;
