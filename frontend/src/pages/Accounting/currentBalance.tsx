import { FC } from "react";
import CurrentBalanceMain from "../../components/Accounting/currentBalance";

const CurrentBalance: FC = () => {
  return <CurrentBalanceMain />;
};

export default CurrentBalance;
