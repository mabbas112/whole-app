import { FC } from "react";
import PaymentDueMain from "../../components/Accounting/paymentDue";

const PaymentDue: FC = () => {
  return <PaymentDueMain />;
};

export default PaymentDue;
