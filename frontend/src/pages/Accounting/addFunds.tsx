import { FC } from "react";
import AddFundsMain from "../../components/Accounting/addFunds";

const AddFunds: FC = () => {
  return <AddFundsMain />;
};

export default AddFunds;
