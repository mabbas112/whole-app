import { FC } from "react";
import LoginMainForm from "../components/Login";
interface Props {
  name?: string;
}
const Login: FC<Props> = () => {
  return (
    <div className="px-5 flex flex-col gap-4">
      <h4 className="font-bold text-xl ">Log In</h4>
      <LoginMainForm />
    </div>
  );
};

export default Login;
