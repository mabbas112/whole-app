import MainLayout from "../components/Common/layouts/MainLayout";
import NotFound from "../components/NotFound";
import IRoute from "../interfaces/route";
import AddFunds from "../pages/Accounting/addFunds";
import CurrentBalance from "../pages/Accounting/currentBalance";
import PaymentDue from "../pages/Accounting/paymentDue";
import PaymentHistory from "../pages/Accounting/paymentHistory";
import LotsLost from "../pages/Activity/lotsLost";
import LotsWon from "../pages/Activity/lotsWon";
import ProxyBids from "../pages/Activity/proxyBids";
import WatchList from "../pages/Activity/watchList";
import TodayAuctions from "../pages/Auctions/todayAuctions";
import UpcomingAutions from "../pages/Auctions/upcomingAuctions";
import Buy from "../pages/buy";
import DashBoard from "../pages/dashboard";
import Login from "../pages/login";
import Profile from "../pages/profile";
import Sell from "../pages/sell";
import Settings from "../pages/settings";
import Signup from "../pages/signup";

const routes: IRoute[] = [
  {
    layout: MainLayout,
    name: "Main Layout",
    routes: [
      {
        path: "/",
        name: "Dashboard",
        component: DashBoard,
      },
      {
        path: "/login",
        name: "Login",
        component: Login,
      },
      {
        path: "/activity/proxybids",
        name: "Proxy Bids",
        component: ProxyBids,
      },
      {
        path: "/activity/watchlist",
        name: "Watch List",
        component: WatchList,
      },
      {
        path: "/activity/lotswon",
        name: "Lots Won",
        component: LotsWon,
      },
      {
        path: "/activity/lotslost",
        name: "Lots Lost",
        component: LotsLost,
      },
      {
        path: "/signup",
        name: "Signup",
        component: Signup,
      },
      {
        path: "/auctions/today",
        name: "Today's Auction",
        component: TodayAuctions,
      },
      {
        path: "/auctions/upcoming",
        name: "Upcoming Auctions",
        component: UpcomingAutions,
      },
      {
        path: "/accounting/currentbalance",
        name: "Current Balance",
        component: CurrentBalance,
      },
      {
        path: "/accounting/paymentdue",
        name: "Payment Due",
        component: PaymentDue,
      },
      {
        path: "/accounting/paymenthistory",
        name: "Payment History",
        component: PaymentHistory,
      },
      {
        path: "/accounting/addfunds",
        name: "Add Funds",
        component: AddFunds,
      },
      {
        path: "/profile",
        name: "Profile",
        component: Profile,
      },
      {
        path: "/settings",
        name: "Settings",
        component: Settings,
      },
      {
        path: "/buy",
        name: "Buy",
        component: Buy,
      },
      {
        path: "/sell",
        name: "Sell",
        component: Sell,
      },
      {
        path: "*",
        name: "Not Found",
        component: NotFound,
      },
    ],
  },
];

export default routes;
