/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    fontFamily: {
      sans: ["Poppins", "Open Sans", "sans-serif"],
      inter: ["Inter", "sans-serif"],
      neue: ["Bebas Neue", "cursive"],
      aviano: ["Aviano", "sans-serif"],
      poppins: ["Poppins"],
      bebas: ["Bebas Neue"],
    },
    extend: {
      colors: {
        darkBlue: "#253d87",
      },
    },
  },

  plugins: [("@tailwindcss/forms")],
};
